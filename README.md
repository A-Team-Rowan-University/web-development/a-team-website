# a-team-website

The central server used for managing the databases of the ECE Apprengineering Team at Rowan University.

## Gantt Chart
https://live.ganttlab.org/?l=project&p=A-Team-Rowan-University/web-development/a-team-website

## Usage
First, clone this repo:
```
git clone https://github.com/A-Team-Rowan-University/a-team-website
```

Then, cd to it
```
cd a-team-website
```

Build the images with
```
docker-compose build
```

Create and run the containers
```
docker-compose up -d
```

You can stop the running containers with
```
docker-compose stop
```

And start the them again (without rebuilding them) with
```
docker-compose start
```

And finally, remove the containers and images
```
docker-compose down
```

See `docker-compose --help` for more information

Also you can use the debug script for simplicity
```
./debug_run.sh
```

Generate frontend graphql, run from the chemicals/frontend folder
```
elm-graphql http://localhost:8002/api/graphql --base ChemicalsApi --output src --scalar-codecs CustomScalarCodecs
```
