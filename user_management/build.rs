
use std::env;
use std::fs::create_dir;
use std::fs::copy;
use std::process::Command;

const FRONTEND_DIR: &str = "frontend";

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    if out_dir != FRONTEND_DIR {
        create_dir(format!("{}/static", out_dir));

        copy(
            &format!("{}/static/logo.svg", FRONTEND_DIR),
            &format!("{}/static/logo.svg", out_dir)
        ).expect("Could not copy logo.svg");

        copy(
            &format!("{}/static/main.html", FRONTEND_DIR),
            &format!("{}/static/main.html", out_dir)
        ).expect("Could not copy main.html");
    }

    let elm_cmd = env::var("ELM_CMD").unwrap_or("elm".to_owned());

    if !Command::new(elm_cmd).args(&[
        "make",
        &format!("src/Main.elm"),
        "--output",
        &format!("{}/static/main.js", out_dir),
    ]).current_dir(FRONTEND_DIR).status().expect("Could not run elm").success() {
        panic!("Frontend elm failed to compile");
    }
}

