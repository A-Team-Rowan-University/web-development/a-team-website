table! {
    users (id) {
        id -> Unsigned<Bigint>,
        first_name -> Varchar,
        last_name -> Varchar,
        banner_id -> Integer,
        email -> Varchar,
    }
}

table! {
    groups (id) {
        id -> Unsigned<Bigint>,
        name -> Varchar,
    }
}

table! {
    users_groups (user_id, group_id) {
        user_id -> Unsigned<Bigint>,
        group_id -> Unsigned<Bigint>,
    }
}

joinable!(users_groups -> users (user_id));
joinable!(users_groups -> groups (group_id));

allow_tables_to_appear_in_same_query!(users_groups, users, groups);
