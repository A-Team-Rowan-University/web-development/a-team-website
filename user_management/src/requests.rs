use diesel;
use diesel::mysql::MysqlConnection;
use diesel::RunQueryDsl;
use diesel::ExpressionMethods;
use diesel::QueryDsl;

use crate::Error;

use crate::models::{
    NewUser, PartialUser, User, UserList,
};

use crate::schema::users as users_schema;

pub(crate) fn get_users(
    database_connection: &MysqlConnection,
) -> Result<UserList, Error> {
    let users = users_schema::table.load::<User>(database_connection)?;

    let user_list = UserList { users };

    Ok(user_list)
}

pub(crate) fn get_user(id: u64, database_connection: &MysqlConnection) -> Result<User, Error> {
    let mut users = users_schema::table
        .filter(users_schema::id.eq(id))
        .load::<User>(database_connection)?;

    match users.pop() {
        Some(user) => Ok(user),
        None => Err(Error::UserNotFound(id)),
    }
}

pub(crate) fn create_user(
    user: NewUser,
    database_connection: &MysqlConnection,
) -> Result<User, Error> {
    diesel::insert_into(users_schema::table)
        .values(user)
        .execute(database_connection)?;

    let mut inserted_users = users_schema::table
        .filter(diesel::dsl::sql("id = LAST_INSERT_ID()"))
        .load::<User>(database_connection)?;

    if let Some(inserted_user) = inserted_users.pop() {
        Ok(inserted_user)
    } else {
        Err(Error::NewUserInsert)
    }
}

pub(crate) fn update_user(
    id: u64,
    user: PartialUser,
    database_connection: &MysqlConnection,
) -> Result<(), Error> {
    diesel::update(users_schema::table)
        .filter(users_schema::id.eq(id))
        .set(&user)
        .execute(database_connection)?;
    Ok(())
}

pub(crate) fn delete_user(id: u64, database_connection: &MysqlConnection) -> Result<(), Error> {
    diesel::delete(users_schema::table.filter(users_schema::id.eq(id)))
        .execute(database_connection)?;

    Ok(())
}
