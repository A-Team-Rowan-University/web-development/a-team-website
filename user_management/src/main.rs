// Some diesel macros rely on the old way of importing macros
#[macro_use]
extern crate diesel_migrations;

#[macro_use]
extern crate diesel;

mod models;
mod requests;
mod schema;

use std::env;

use diesel::r2d2::ConnectionManager;
use diesel::MysqlConnection;
use r2d2::Pool;

use serde::Serialize;

use rouille::router;
use rouille::Request;
use rouille::Response;

use log::debug;
use log::error;
use log::info;
use log::trace;
use log::warn;

static LOGO_SVG: &'static str = include_str!(concat!(env!("OUT_DIR"), "/static/logo.svg"));
static MAIN_HTML: &'static str = include_str!(concat!(env!("OUT_DIR"), "/static/main.html"));
static MAIN_JS: &'static str = include_str!(concat!(env!("OUT_DIR"), "/static/main.js"));

enum Error {
    CouldNotBuildConnectionPool,
    ConnectionPoolTimeout,

    GoogleSignIn(google_signin::Error),
    GoogleUserNoEmail,
    GoogleUserEmailNotFound(String),

    InvalidBody(serde_json::Error),
    NoBody,

    UserNotFound(u64),
    UserWithEmailAlreadyExists(String),
    NewUserInsert,
    NoChanges,
    FirstUserAlreadyRun,

    Unknown(Box<dyn std::error::Error>),
}

impl Error {
    fn code(&self) -> u16 {
        match self {
            Error::CouldNotBuildConnectionPool => 0,
            Error::ConnectionPoolTimeout => 1,
            Error::GoogleSignIn(_) => 10,
            Error::GoogleUserNoEmail => 11,
            Error::GoogleUserEmailNotFound(_) => 12,
            Error::InvalidBody(_) => 20,
            Error::NoBody => 21,
            Error::UserNotFound(_) => 30,
            Error::UserWithEmailAlreadyExists(_) => 31,
            Error::NewUserInsert => 32,
            Error::NoChanges => 33,
            Error::FirstUserAlreadyRun => 34,
            Error::Unknown(_) => 99,
        }
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::CouldNotBuildConnectionPool => write!(
                f,
                "Could not get enough database connections to fill the connection pool"
            ),

            Error::ConnectionPoolTimeout => write!(
                f,
                "Timed out trying to get a connection from the connection pool"
            ),

            Error::GoogleSignIn(e) => match e {
                google_signin::Error::DecodeJson(e) => {
                    write!(f, "Google sign in returned invalid JSON: {}", e)
                }

                google_signin::Error::ConnectionError(e) => {
                    write!(f, "Could not connect to Google to validate token: {}", e)
                }

                _ => write!(f, "Invalid Google Signin token"),
            },

            Error::GoogleUserNoEmail => {
                write!(f, "Google did not provide an email for the signed-in user")
            }

            Error::GoogleUserEmailNotFound(email) => write!(
                f,
                "The email provided by Google does not match a user in the database: {}",
                email
            ),

            Error::InvalidBody(e) => write!(f, "{}", e),

            Error::NoBody => write!(f, "No body was provided with the request"),

            Error::UserNotFound(id) => write!(f, "The requeted user was not found: {}", id),

            Error::UserWithEmailAlreadyExists(email) => write!(
                f,
                "A user with the provided email already exists: {}",
                email
            ),

            Error::NewUserInsert => write!(f, "Could not insert a new user"),

            Error::NoChanges => write!(f, "No changes were requested"),

            Error::FirstUserAlreadyRun => write!(
                f,
                "Tried to create first user, but there are already users in the database"
            ),

            Error::Unknown(e) => write!(f, "An unknown error occurred: {}", e),
        }
    }
}

impl From<r2d2::Error> for Error {
    fn from(_e: r2d2::Error) -> Error {
        Error::ConnectionPoolTimeout
    }
}

impl From<google_signin::Error> for Error {
    fn from(e: google_signin::Error) -> Error {
        Error::GoogleSignIn(e)
    }
}

impl From<diesel::result::Error> for Error {
    fn from(e: diesel::result::Error) -> Error {
        match e {
            diesel::result::Error::QueryBuilderError(_e) => Error::NoChanges,
            _ => Error::Unknown(Box::new(e)),
        }
    }
}

#[derive(Serialize, Debug)]
struct ErrorResponse {
    error_code: u16,
    error_description: String,
    http_code: u16,
}

impl From<Error> for Response {
    fn from(e: Error) -> Response {
        let http_code = match e {
            Error::CouldNotBuildConnectionPool => 500,
            Error::ConnectionPoolTimeout => 500,
            Error::GoogleSignIn(_) => 401,
            Error::GoogleUserNoEmail => 401,
            Error::GoogleUserEmailNotFound(_) => 401,
            Error::InvalidBody(_) => 400,
            Error::NoBody => 400,
            Error::UserNotFound(_) => 404,
            Error::UserWithEmailAlreadyExists(_) => 409,
            Error::NewUserInsert => 500,
            Error::NoChanges => 400,
            Error::FirstUserAlreadyRun => 401,
            Error::Unknown(_) => 500,
        };

        let error_response = ErrorResponse {
            error_code: e.code(),
            error_description: e.to_string(),
            http_code,
        };

        Response::json(&error_response).with_status_code(http_code)
    }
}

fn validate_token(
    id_token: &str,
    database_connection: &MysqlConnection,
) -> Result<models::User, Error> {
    let mut client = google_signin::Client::new();
    client.audiences.push(String::from(
        "918184954544-jm1aufr31fi6sdjs1140p7p3rouaka14.apps.googleusercontent.com",
    ));

    let id_info = client.verify(id_token)?;

    debug!("Validated token: {:?}", id_info);

    if let Some(email) = id_info.email {
        let users = requests::get_users(database_connection)?;

        let user = users.users.into_iter().find(|u| u.email == email);

        if let Some(user) = user {
            Ok(user)
        } else {
            Err(Error::GoogleUserEmailNotFound(email))
        }
    } else {
        Err(Error::GoogleUserNoEmail)
    }
}

fn first_user(id_token: &str, database_connection: &MysqlConnection) -> Result<(), Error> {
    let users = requests::get_users(database_connection)?.users.len();

    if users == 0 {
        let mut client = google_signin::Client::new();
        client.audiences.push(String::from(
            "918184954544-jm1aufr31fi6sdjs1140p7p3rouaka14.apps.googleusercontent.com",
        ));

        let info = client.verify(id_token)?;

        debug!("Token verified: {:?}", info);

        let email = match info.email {
            Some(email) => email,
            None => {
                return Err(Error::GoogleUserNoEmail);
            }
        };

        let new_user = models::NewUser {
            first_name: info
                .given_name
                .unwrap_or("Not supplied by Google".to_owned()),
            last_name: info
                .family_name
                .unwrap_or("Not supplied by Google".to_owned()),
            email,
            banner_id: 0,
        };

        debug!("New user: {:#?}", new_user);

        Ok(())
    } else {
        warn!("First user request attempted, but a user has already been setup.");
        Err(Error::FirstUserAlreadyRun)
    }
}

embed_migrations!("migrations");

fn init_database(url: &str) -> Result<Pool<ConnectionManager<MysqlConnection>>, Error> {
    info!("Connecting to database");
    let manager = ConnectionManager::new(url);

    let pool = Pool::builder().max_size(15).build(manager);

    let pool = match pool {
        Ok(p) => p,
        Err(_e) => return Err(Error::CouldNotBuildConnectionPool),
    };

    info!("Running migrations");
    if let Err(e) = embedded_migrations::run(&pool.get()?) {
        warn!("Could not run migrations: {}", e);
    }

    Ok(pool)
}

fn main() {
    let logging_config = simplelog::ConfigBuilder::new()
        .add_filter_ignore("hyper".to_string())
        .add_filter_ignore("rustls".to_string())
        .build();

    simplelog::SimpleLogger::init(simplelog::LevelFilter::Trace, logging_config)
        .expect("Could not initialize logging");

    info!("ConnectionManager to database");

    let database_url = match env::var("DATABASE_URL") {
        Ok(url) => url,
        Err(e) => {
            error!("Could not read DATABASE_URL environment variable: {}", e);
            return;
        }
    };

    let connection_pool = match init_database(&database_url) {
        Ok(pool) => pool,
        Err(e) => {
            error!("Could not connect to the database: {}", e);
            return;
        }
    };

    info!("Connected to database, starting server");

    rouille::start_server("0.0.0.0:8000", move |request| {
        debug!(
            "Handling request {} {} from {}",
            request.method(),
            request.raw_url(),
            request.remote_addr(),
        );

        if request.method() == "OPTIONS" {
            Response::text("")
                .with_additional_header(
                    "Access-Control-Allow-Methods",
                    "POST, GET, DELETE, OPTIONS, PUT",
                )
                .with_additional_header("Access-Control-Allow-Origin", "*")
                .with_additional_header(
                    "Access-Control-Allow-Headers",
                    "X-PINGOTHER, Content-Type, id_token",
                )
                .with_additional_header("Access-Control-Max-Age", "86400")
        } else {
            let response = handle_request(request, connection_pool.clone());

            response.with_additional_header("Access-Control-Allow-Origin", "*")
        }
    });
}

fn handle_request(
    request: &Request,
    connection_pool: Pool<ConnectionManager<MysqlConnection>>,
) -> Response {
    let requested_user = if let Some(id_token) = request.header("id_token") {
        let database_connection = match connection_pool.get() {
            Ok(c) => c,
            Err(_e) => {
                error!("Could not lock database");
                return Response::from(Error::ConnectionPoolTimeout);
            }
        };
        if request.url() == "/first_user" {
            return match first_user(id_token, &database_connection) {
                Ok(()) => Response::empty_204(),
                Err(err) => rouille::Response::from(err),
            };
        } else {
            match validate_token(id_token, &database_connection) {
                Ok(user) => Some(user),
                Err(e) => {
                    warn!("Failed to verify user: {}", e.to_string());
                    return rouille::Response::from(e);
                }
            }
        }
    } else {
        None
    };

    if let Some(api_request) = request.remove_prefix("/api/v1") {
        let database_connection = match connection_pool.get() {
            Ok(c) => c,
            Err(_e) => {
                error!("Could not lock database");
                return Response::from(Error::ConnectionPoolTimeout);
            }
        };

        router!(api_request,
            (GET) (/users) => {
                match requests::get_users(&database_connection) {
                    Ok(user_list) => Response::json(&user_list),
                    Err(e) => e.into(),
                }
            },

            (GET) (/users/{id: u64}) => {
                match requests::get_user(id, &database_connection) {
                    Ok(user) => Response::json(&user),
                    Err(e) => e.into(),
                }
            },

            (GET) (/users/current) => {
                Response::json(&requested_user)
            },

            (POST) (/users) => {
                let request_body = match request.data() {
                    Some(d) => d,
                    None => return Error::NoBody.into()
                };

                let new_user: models::NewUser = match serde_json::from_reader(request_body) {
                    Ok(u) => u,
                    Err(e) => return Error::InvalidBody(e).into(),
                };

                match requests::create_user(new_user, &database_connection) {
                    Ok(user) => Response::json(&user),
                    Err(e) => e.into(),
                }
            },

            (PUT) (/users/{id: u64}) => {
                let request_body = match request.data() {
                    Some(d) => d,
                    None => return Error::NoBody.into()
                };

                let partial_user: models::PartialUser= match serde_json::from_reader(request_body) {
                    Ok(u) => u,
                    Err(e) => return Error::InvalidBody(e).into(),
                };

                match requests::update_user(id, partial_user, &database_connection) {
                    Ok(()) => Response::empty_204(),
                    Err(e) => e.into(),
                }
            },

            (DELETE) (/users/{id: u64}) => {
                match requests::delete_user(id, &database_connection) {
                    Ok(()) => Response::empty_204(),
                    Err(e) => e.into(),
                }
            },

            _ => {
                Response::empty_404()
            }
        )
    } else if let Some(graphql_request) = request.remove_prefix("/api/graphql") {
        let request_body = match graphql_request.data() {
            Some(d) => d,
            None => return Error::NoBody.into(),
        };

        let juniper_request: juniper::http::GraphQLRequest =
            match serde_json::from_reader(request_body) {
                Ok(r) => r,
                Err(e) => return Error::InvalidBody(e).into(),
            };

        trace!(
            "Handling graphql request from user: {:?} : {:?}",
            requested_user,
            juniper_request
        );

        let root_node = models::Schema::new(models::Query, models::Mutation);

        let context = models::Context {
            user: requested_user,
            pool: connection_pool,
        };

        let juniper_response = juniper_request.execute(&root_node, &context);

        Response::json(&juniper_response)
    } else if let Some(_graphiql_request) = request.remove_prefix("/api/graphiql") {
        let html = juniper::graphiql::graphiql_source("/api/graphql");
        Response::html(html)
    } else {
        match request.url().as_ref() {
            "/main.js" => Response::text(MAIN_JS),
            "/logo.svg" => Response::svg(LOGO_SVG),
            "/favicon.ico" => Response::svg(LOGO_SVG),
            _ => Response::html(MAIN_HTML),
        }
    }
}
