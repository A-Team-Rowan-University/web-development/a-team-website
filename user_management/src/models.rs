use diesel::r2d2::ConnectionManager;
use diesel::AsChangeset;
use diesel::ExpressionMethods;
use diesel::Insertable;
use diesel::MysqlConnection;
use diesel::QueryDsl;
use diesel::Queryable;
use diesel::RunQueryDsl;
use diesel_derive_newtype::DieselNewType;
use juniper;
use juniper::parser::ParseError;
use juniper::parser::ScalarToken;
use juniper::parser::Token;
use juniper::FieldError;
use juniper::FieldResult;
use juniper::GraphQLInputObject;
use juniper::GraphQLObject;
use juniper::InputValue;
use juniper::ParseScalarResult;
use juniper::ParseScalarValue;
use juniper::Value;
use r2d2::Pool;
use serde::Deserialize;
use serde::Serialize;

use crate::schema::users;

#[derive(Clone)]
pub struct Context {
    pub user: Option<User>,
    pub pool: Pool<ConnectionManager<MysqlConnection>>,
}

impl juniper::Context for Context {}

pub struct Query;

#[juniper::object(Context=Context)]
impl Query {
    fn users(context: &Context) -> FieldResult<Vec<User>> {
        let connection = context.pool.get()?;

        let mut users = users::table.load::<User>(&connection)?;

        Ok(users)
    }

    fn user(context: &Context, id: String) -> FieldResult<User> {
        let connection = context.pool.get()?;

        let mut users = users::table
            .filter(users::id.eq(id.parse::<u64>()?))
            .load::<User>(&connection)?;

        match users.pop() {
            Some(user) => Ok(user),
            None => Err(FieldError::new("Could not find user", Value::null())),
        }
    }

    fn current(context: &mut Context) -> FieldResult<User> {
        match &context.user {
            Some(user) => Ok(user.clone()),
            None => Err(FieldError::new("Could not find user", Value::null())),
        }
    }
}

pub struct Mutation;

#[juniper::object(Context=Context)]
impl Mutation {
    fn create(context: &Context, user: NewUser) -> FieldResult<User> {
        let connection = context.pool.get()?;

        diesel::insert_into(users::table)
            .values(user)
            .execute(&connection)?;

        let mut inserted_users = users::table
            .filter(diesel::dsl::sql("id = LAST_INSERT_ID()"))
            .load::<User>(&connection)?;

        match inserted_users.pop() {
            Some(inserted_user) => Ok(inserted_user),
            None => Err(FieldError::new("Could not insert user", Value::null())),
        }
    }

    fn update(context: &Context, id: UserId, user: PartialUser) -> FieldResult<User> {
        let connection = context.pool.get()?;

        diesel::update(users::table)
            .filter(users::id.eq(id))
            .set(&user)
            .execute(&connection)?;

        let mut updated_users = users::table
            .filter(users::id.eq(id))
            .load::<User>(&connection)?;

        match updated_users.pop() {
            Some(updated_user) => Ok(updated_user),
            None => Err(FieldError::new("User could not be updated", Value::null())),
        }
    }

    fn delete(context: &Context, id: UserId) -> FieldResult<User> {
        let connection = context.pool.get()?;

        let mut users_to_delete = users::table
            .filter(users::id.eq(id))
            .load::<User>(&connection)?;

        let user_to_delete = match users_to_delete.pop() {
            Some(user) => user,
            None => {
                return Err(FieldError::new(
                    "Could not find user to delete",
                    Value::null(),
                ))
            }
        };

        diesel::delete(users::table.filter(users::id.eq(id))).execute(&connection)?;

        Ok(user_to_delete)
    }
}

pub type Schema = juniper::RootNode<'static, Query, Mutation>;

#[derive(Debug, DieselNewType, Serialize, Deserialize, Copy, Clone)]
#[serde(transparent)]
pub struct UserId(u64);

juniper::graphql_scalar!(UserId as "UserId" where Scalar = <S> {
    description: "A user id"

    resolve(&self) -> Value {
        Value::scalar(self.0.to_string())
    }

    from_input_value(v: &InputValue) -> Option<UserId> {
        v.as_scalar_value().and_then(|s: &String| s.parse::<u64>().ok()).map(|s| UserId(s))
    }

    from_str<'a>(value: ScalarToken<'a>) -> ParseScalarResult<'a, S> {
        if let ScalarToken::String(value) = value {
            Ok(S::from(value.to_owned()))
        } else {
            Err(ParseError::UnexpectedToken(Token::Scalar(value)))
        }
    }
});

#[derive(Queryable, Serialize, Deserialize, GraphQLObject, Debug, Clone)]
pub struct User {
    pub id: UserId,
    pub first_name: String,
    pub last_name: String,
    pub banner_id: i32,
    pub email: String,
}

#[derive(Insertable, Serialize, Deserialize, GraphQLInputObject, Debug)]
#[table_name = "users"]
pub struct NewUser {
    pub first_name: String,
    pub last_name: String,
    pub banner_id: i32,
    pub email: String,
}

#[derive(Debug, AsChangeset, Serialize, Deserialize, GraphQLInputObject)]
#[table_name = "users"]
pub struct PartialUser {
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub banner_id: Option<i32>,
    pub email: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserList {
    pub users: Vec<User>,
}
