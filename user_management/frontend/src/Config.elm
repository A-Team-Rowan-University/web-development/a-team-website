module Config exposing (apiUrl, graphqlUrl, staticUrl)

import Url.Builder as B


graphqlUrl : String
graphqlUrl =
    B.absolute [ "api", "graphql" ] []


apiUrl : String
apiUrl =
    --B.crossOrigin "http://localhost" [ "api", "v1" ] []
    B.absolute [ "api", "v1" ] []


staticUrl : String
staticUrl =
    B.absolute [ "" ] []
