module CustomScalarCodecs exposing (..)

import Json.Decode as Decode
import Json.Encode as Encode
import UsersApi.Scalar


type alias UserId =
    Int


codecs : UsersApi.Scalar.Codecs UserId
codecs =
    UsersApi.Scalar.defineCodecs
        { codecUserId =
            { encoder = \raw -> raw |> String.fromInt |> Encode.string
            , decoder =
                Decode.string
                    |> Decode.map String.toInt
                    |> Decode.andThen
                        (\maybeParsedId ->
                            case maybeParsedId of
                                Just parsedId ->
                                    Decode.succeed parsedId

                                Nothing ->
                                    Decode.fail "Could not parse Id as an Int"
                        )
            }
        }
