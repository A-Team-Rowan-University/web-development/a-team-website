module Users.Users exposing
    ( Id
    , User
    , decoder
    , listDecoder
    , manyUrl
    , mutationCreate
    , queryMany
    , querySingle
    , singleUrl
    , view
    , viewEditableInt
    , viewEditableText
    , viewList
    )

import Config exposing (..)
import Dict exposing (Dict)
import Graphql.Operation exposing (RootMutation, RootQuery)
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet)
import Html exposing (Html, a, button, div, input, p, span, text)
import Html.Attributes exposing (class, href, readonly, value)
import Html.Events exposing (onClick, onInput)
import Json.Decode as D
import Url.Builder as B
import UsersApi.Mutation as Mutation
import UsersApi.Object.User as ApiUser
import UsersApi.Query as Query


type alias Id =
    Int


type alias User =
    { id : Id
    , first_name : String
    , last_name : String
    , email : String
    , banner_id : Int
    }


type alias NewUser =
    { first_name : String
    , last_name : String
    , email : String
    , banner_id : Int
    }


queryMany : SelectionSet (List User) RootQuery
queryMany =
    Query.users (SelectionSet.map5 User ApiUser.id ApiUser.firstName ApiUser.lastName ApiUser.email ApiUser.bannerId)


querySingle : Id -> SelectionSet User RootQuery
querySingle id =
    Query.user { id = String.fromInt id } (SelectionSet.map5 User ApiUser.id ApiUser.firstName ApiUser.lastName ApiUser.email ApiUser.bannerId)


mutationCreate : NewUser -> SelectionSet User RootMutation
mutationCreate user =
    Mutation.createUser
        { user =
            { firstName = user.first_name
            , lastName = user.last_name
            , email = user.email
            , bannerId = user.banner_id
            }
        }
        (SelectionSet.map5 User ApiUser.id ApiUser.firstName ApiUser.lastName ApiUser.email ApiUser.bannerId)


manyUrl : String
manyUrl =
    B.relative [ apiUrl, "users" ] []


singleUrl : Id -> String
singleUrl user_id =
    B.relative [ apiUrl, "users", String.fromInt user_id ] []



-- BEGIN New User


viewList : Dict Id User -> Html msg
viewList users =
    div []
        [ p [ class "title has-text-centered" ] [ text "Users" ]
        , div [ class "columns" ]
            [ div [ class "column is-one-fifth" ]
                [ p [ class "title is-4 has-text-centered" ] [ text "Search" ]
                , p [ class "has-text-centered" ] [ text "Working on it :)" ]
                ]
            , div [ class "column" ]
                [ div [] (List.map view (Dict.values users))
                , a [ class "button is-primary", href "/users/new" ] [ text "New User" ]
                ]
            ]
        ]


view : User -> Html msg
view user =
    a
        [ class "box"
        , href (B.relative [ "users", String.fromInt user.id ] [])
        ]
        [ p [ class "title is-5" ]
            [ text (user.first_name ++ " " ++ user.last_name) ]
        , p [ class "subtitle is-5 columns" ]
            [ span [ class "column" ]
                [ text ("Email: " ++ user.email) ]
            , span [ class "column" ]
                [ text ("Banner ID: " ++ String.fromInt user.banner_id) ]
            ]
        ]


viewEditableText : String -> Maybe String -> (String -> msg) -> msg -> Html msg
viewEditableText defaultText editedText onEdit onReset =
    case editedText of
        Nothing ->
            div [ class "field has-addons" ]
                [ div [ class "control is-expanded" ]
                    [ input
                        [ class "input"
                        , value defaultText
                        , readonly True
                        ]
                        []
                    ]
                , div [ class "control" ]
                    [ button
                        [ class "button"
                        , onClick (onEdit defaultText)
                        ]
                        [ text "Edit" ]
                    ]
                ]

        Just edited ->
            div [ class "field has-addons" ]
                [ div [ class "control is-expanded" ]
                    [ input
                        [ class "input is-focused"
                        , value edited
                        , onInput onEdit
                        ]
                        []
                    ]
                , div [ class "control" ]
                    [ button
                        [ class "button is-danger"
                        , onClick onReset
                        ]
                        [ text "Reset" ]
                    ]
                ]


viewEditableInt : Int -> Maybe Int -> (Maybe Int -> msg) -> msg -> Html msg
viewEditableInt default edited onEdit onReset =
    case edited of
        Nothing ->
            div [ class "field has-addons" ]
                [ div [ class "control is-expanded" ]
                    [ input
                        [ class "input"
                        , value (String.fromInt default)
                        , readonly True
                        ]
                        []
                    ]
                , div [ class "control" ]
                    [ button
                        [ class "button"
                        , onClick (onEdit (Just default))
                        ]
                        [ text "Edit" ]
                    ]
                ]

        Just edit ->
            div [ class "field has-addons" ]
                [ div [ class "control is-expanded" ]
                    [ input
                        [ class "input is-focused"
                        , value (String.fromInt edit)
                        , onInput (\s -> onEdit (String.toInt s))
                        ]
                        []
                    ]
                , div [ class "control" ]
                    [ button
                        [ class "button is-danger"
                        , onClick onReset
                        ]
                        [ text "Reset" ]
                    ]
                ]


decoder : D.Decoder User
decoder =
    D.map5 User
        (D.field "id" D.int)
        (D.field "first_name" D.string)
        (D.field "last_name" D.string)
        (D.field "email" D.string)
        (D.field "banner_id" D.int)


listDecoder : D.Decoder (List User)
listDecoder =
    D.field "users" (D.list decoder)
