module Users.New exposing (Msg, State, init, update, view)

import Config exposing (..)
import Graphql.Http
import Html exposing (Html, button, div, input, p, span, text)
import Html.Attributes exposing (class, type_, value)
import Html.Events exposing (onClick, onInput)
import Json.Encode
import Network exposing (RequestChange(..))
import Response exposing (Response)
import Users.Users as User


type alias NewUser r =
    { r
        | first_name : String
        , last_name : String
        , banner_id : Int
        , email : String
    }


type alias State =
    { first_name : String
    , last_name : String
    , banner_id : Int
    , email : String
    }


init : State
init =
    { first_name = ""
    , last_name = ""
    , banner_id = 0
    , email = ""
    }


type Msg
    = EditFirstName String
    | EditLastName String
    | EditEmail String
    | EditBannerId (Maybe Int)
    | Submit
    | Submitted (Result (Graphql.Http.Error User.User) User.User)


update : String -> State -> Msg -> Response State Msg
update id_token state msg =
    case msg of
        EditFirstName first_name ->
            { state = { state | first_name = first_name }
            , cmd = Cmd.none
            , requests = []
            , done = False
            , reload = False
            , errors = []
            }

        EditLastName last_name ->
            { state = { state | last_name = last_name }
            , cmd = Cmd.none
            , requests = []
            , done = False
            , reload = False
            , errors = []
            }

        EditBannerId banner_id ->
            case banner_id of
                Just id ->
                    { state = { state | banner_id = id }
                    , cmd = Cmd.none
                    , requests = []
                    , done = False
                    , reload = False
                    , errors = []
                    }

                Nothing ->
                    { state = state
                    , cmd = Cmd.none
                    , requests = []
                    , done = False
                    , reload = False
                    , errors = []
                    }

        EditEmail email ->
            { state = { state | email = email }
            , cmd = Cmd.none
            , requests = []
            , done = False
            , reload = False
            , errors = []
            }

        Submit ->
            { state = state
            , cmd =
                User.mutationCreate state
                    |> Graphql.Http.mutationRequest graphqlUrl
                    |> Graphql.Http.send Submitted
            , requests = [ User.manyUrl |> AddRequest ]
            , done = False
            , reload = False
            , errors = []
            }

        Submitted result ->
            case result of
                Ok _ ->
                    { state = init
                    , cmd = Cmd.none
                    , requests = [ User.manyUrl |> RemoveRequest ]
                    , done = True
                    , reload = False
                    , errors = []
                    }

                Err _ ->
                    { state = state
                    , cmd = Cmd.none
                    , requests = [ User.manyUrl |> RemoveRequest ]
                    , done = False
                    , reload = False
                    , errors = []
                    }


view : State -> Html Msg
view state =
    div []
        [ p [ class "title has-text-centered" ]
            [ text "New User" ]
        , p [ class "columns" ]
            [ span [ class "column" ]
                [ p [ class "subtitle has-text-centered" ]
                    [ text "User Details" ]
                , div [ class "box" ]
                    [ span [] [ text "First Name: " ]
                    , input
                        [ class "input"
                        , value state.first_name
                        , onInput EditFirstName
                        ]
                        []
                    ]
                , div [ class "box" ]
                    [ span [] [ text "Last Name: " ]
                    , input
                        [ class "input"
                        , value state.last_name
                        , onInput EditLastName
                        ]
                        []
                    ]
                , div [ class "box" ]
                    [ span [] [ text "Email: " ]
                    , input
                        [ class "input"
                        , value state.email
                        , onInput EditEmail
                        ]
                        []
                    ]
                , div [ class "box" ]
                    [ span [] [ text "Banner ID: " ]
                    , input
                        [ class "input"
                        , type_ "number"
                        , value (String.fromInt state.banner_id)
                        , onInput
                            (\s -> String.toInt s |> EditBannerId)
                        ]
                        []
                    ]
                , button
                    [ class "button is-primary"
                    , onClick Submit
                    ]
                    [ text "Submit new user" ]
                ]
            , div [ class "column" ] []
            ]
        ]


newEncoder : NewUser r -> Json.Encode.Value
newEncoder user =
    Json.Encode.object
        [ ( "first_name", Json.Encode.string user.first_name )
        , ( "last_name", Json.Encode.string user.last_name )
        , ( "banner_id", Json.Encode.int user.banner_id )
        , ( "email", Json.Encode.string user.email )
        ]
