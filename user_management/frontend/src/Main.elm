port module Main exposing
    ( Model
    , Msg(..)
    , init
    , main
    , subscriptions
    , update
    , view
    )

import Browser
import Browser.Navigation as Nav
import Config exposing (..)
import Dict exposing (Dict)
import Errors
import Graphql.Http
import Graphql.Operation exposing (RootMutation, RootQuery)
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Json.Decode as D
import Json.Encode as E
import Platform.Cmd
import Platform.Sub
import Session exposing (Session, googleUserDecoder, idToken, isValidated)
import Table exposing (defaultCustomizations)
import Task
import Time
import Url
import Url.Builder as B
import Url.Parser as P exposing ((</>))
import UsersApi.Mutation as Mutation
import UsersApi.Object.User as ApiUser
import UsersApi.Query as Query


main =
    Browser.application
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }


subscriptions : Model -> Sub Msg
subscriptions _ =
    signIn SignedIn



-- Ports


port signIn : (E.Value -> msg) -> Sub msg


port signOut : () -> Cmd msg



-- Queries and Mutations


queryMany : SelectionSet (List User) RootQuery
queryMany =
    Query.users (SelectionSet.map5 User ApiUser.id ApiUser.firstName ApiUser.lastName ApiUser.email ApiUser.bannerId)


querySingle : Id -> SelectionSet User RootQuery
querySingle id =
    Query.user { id = String.fromInt id } (SelectionSet.map5 User ApiUser.id ApiUser.firstName ApiUser.lastName ApiUser.email ApiUser.bannerId)


queryCurrent : SelectionSet User RootQuery
queryCurrent =
    Query.current (SelectionSet.map5 User ApiUser.id ApiUser.firstName ApiUser.lastName ApiUser.email ApiUser.bannerId)


mutationCreate : NewUser -> SelectionSet User RootMutation
mutationCreate user =
    Mutation.create
        { user =
            { firstName = user.first_name
            , lastName = user.last_name
            , email = user.email
            , bannerId = user.banner_id
            }
        }
        (SelectionSet.map5 User ApiUser.id ApiUser.firstName ApiUser.lastName ApiUser.email ApiUser.bannerId)


users_table_config : Table.Config User Msg
users_table_config =
    Table.customConfig
        { toId = \user -> String.fromInt user.id
        , toMsg = UserTableState
        , columns =
            [ Table.stringColumn "First Name" .first_name
            , Table.stringColumn "Last Name" .last_name
            , Table.stringColumn "Email" .email
            , Table.intColumn "Banner ID" .banner_id
            ]
        , customizations = { defaultCustomizations | tableAttrs = [ class "table is-fullwidth is-hoverable is-striped" ] }
        }



-- MODEL


type alias Id =
    Int


type alias User =
    { id : Id
    , first_name : String
    , last_name : String
    , email : String
    , banner_id : Int
    }


type alias NewUser =
    { first_name : String
    , last_name : String
    , email : String
    , banner_id : Int
    }


type alias PartialUser =
    { first_name : Maybe String
    , last_name : Maybe String
    , email : Maybe String
    , banner_id : Maybe Int
    }


type Route
    = Home
    | Users
    | UserDetail PartialUser Id
    | UserNew NewUser
    | NotFound


routeParser : P.Parser (Route -> a) a
routeParser =
    P.oneOf
        [ P.map Home P.top
        , P.map Users (P.s "users")
        , P.map (UserDetail { first_name = Nothing, last_name = Nothing, email = Nothing, banner_id = Nothing }) (P.s "users" </> P.int)
        , P.map (UserNew { first_name = "", last_name = "", email = "", banner_id = 0 }) (P.s "users" </> P.s "new")
        ]


type alias Model =
    { navkey : Nav.Key
    , route : Route
    , session : Session Id
    , timezone : Maybe Time.Zone
    , users : Dict Id User
    , users_table : Table.State
    , burger_open : Bool
    }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( { navkey = key
      , route = Maybe.withDefault NotFound (P.parse routeParser url)
      , session = Session.NotSignedIn
      , timezone = Nothing
      , users = Dict.empty
      , users_table = Table.initialSort "First Name"
      , burger_open = False
      }
    , Task.perform GotTimezone Time.here
    )



-- UPDATE


type Msg
    = SignedIn E.Value
    | Validated (Result (Graphql.Http.Error User) User)
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | GotTimezone Time.Zone
    | GotUsers (Result (Graphql.Http.Error (List User)) (List User))
    | GotUser Id (Result (Graphql.Http.Error User) User)
    | UserTableState Table.State
    | Updated (Result Errors.Error ())
    | BurgerToggle
    | SignOut


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SignedIn user_json ->
            case D.decodeValue googleUserDecoder user_json of
                Ok google_user ->
                    ( { model
                        | session = Session.SignedIn google_user
                      }
                    , queryCurrent
                        |> Graphql.Http.queryRequest graphqlUrl
                        |> Graphql.Http.withHeader "id_token" google_user.id_token
                        |> Graphql.Http.send Validated
                    )

                Err e ->
                    ( { model | session = Session.GoogleError e }
                    , Cmd.none
                    )

        Validated user_result ->
            case model.session of
                Session.SignedIn google_user ->
                    case user_result of
                        Ok user ->
                            let
                                session =
                                    Session.Validated user.id google_user
                            in
                            ( { model | session = session, users = Dict.insert user.id user model.users }
                            , loadData session model.route
                            )

                        Err e ->
                            ( { model
                                | session = Session.NotSignedIn
                              }
                            , signOut ()
                            )

                _ ->
                    ( model, Cmd.none )

        GotTimezone zone ->
            ( { model | timezone = Just zone }, Cmd.none )

        GotUsers users_result ->
            ( case users_result of
                Ok users ->
                    { model | users = Dict.fromList (List.map (\u -> ( u.id, u )) users) }

                Err _ ->
                    model
            , Cmd.none
            )

        GotUser id user_result ->
            ( case user_result of
                Ok user ->
                    { model | users = Dict.insert user.id user model.users }

                Err _ ->
                    model
            , Cmd.none
            )

        UserTableState state ->
            ( { model | users_table = state }, Cmd.none )

        Updated _ ->
            ( model, loadData model.session model.route )

        LinkClicked request ->
            case request of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.navkey (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            case P.parse routeParser url of
                Nothing ->
                    ( { model | route = NotFound }, Cmd.none )

                Just route ->
                    ( { model | route = route }, loadData model.session route )

        BurgerToggle ->
            ( { model | burger_open = not model.burger_open }, Cmd.none )

        SignOut ->
            ( model, Cmd.batch [ signOut (), Nav.load "/" ] )


loadData :
    Session Id
    -> Route
    -> Cmd Msg
loadData session route =
    case route of
        Home ->
            Cmd.none

        Users ->
            case idToken session of
                Just id_token ->
                    queryMany
                        |> Graphql.Http.queryRequest graphqlUrl
                        |> Graphql.Http.withHeader "id_token" id_token
                        |> Graphql.Http.send GotUsers

                Nothing ->
                    Cmd.none

        UserDetail _ user_id ->
            case idToken session of
                Just id_token ->
                    querySingle user_id
                        |> Graphql.Http.queryRequest graphqlUrl
                        |> Graphql.Http.withHeader "id_token" id_token
                        |> Graphql.Http.send (GotUser user_id)

                Nothing ->
                    Cmd.none

        UserNew _ ->
            Cmd.none

        NotFound ->
            Cmd.none


viewPage : Model -> Html Msg
viewPage model =
    case model.route of
        Users ->
            viewUserList model.users_table model.users

        UserDetail editted_user user_id ->
            h1 [] [ text "User detail!" ]

        UserNew new_user ->
            h1 [] [ text "New user!" ]

        Home ->
            h1 [] [ text "Welcome to the A-Team!" ]

        NotFound ->
            h1 [] [ text "Page not found!" ]


view : Model -> Browser.Document Msg
view model =
    { title = "User Management"
    , body =
        [ div []
            [ nav [ class "navbar", class "is-primary" ]
                [ div [ class "navbar-brand" ]
                    [ a [ class "navbar-item", href "/" ]
                        [ img
                            [ src
                                (B.absolute [ "logo.svg" ] [])
                            ]
                            []
                        ]
                    , a
                        [ attribute "role" "button"
                        , href ""
                        , class "navbar-burger"
                        , class "burger"
                        , attribute "aria-label" "menu"
                        , attribute "aria-expanded" "false"
                        , attribute "data-target" "navbar"
                        , onClick BurgerToggle
                        , classList [ ( "is-active", model.burger_open ) ]
                        ]
                        [ span [ attribute "aria-hidden" "true" ] []
                        , span [ attribute "aria-hidden" "true" ] []
                        , span [ attribute "aria-hidden" "true" ] []
                        ]
                    ]
                , div
                    [ id "navbar"
                    , class "navbar-menu"
                    , classList [ ( "is-active", model.burger_open ) ]
                    ]
                    [ div [ class "navbar-start" ]
                        [ a [ class "navbar-item", href "/" ]
                            [ text "Home" ]
                        , a [ class "navbar-item", href "/users" ]
                            [ text "Users" ]
                        ]
                    , div [ class "navbar-end" ]
                        [ div [ class "navbar-item has-dropdown is-hoverable" ]
                            [ div [ class "navbar-link" ]
                                [ case model.session of
                                    Session.Validated user_id google_user ->
                                        case Dict.get user_id model.users of
                                            Just user ->
                                                case google_user.image_url of
                                                    Just image_url ->
                                                        div
                                                            [ class "image is-32x32"
                                                            , class "level-item"
                                                            ]
                                                            [ img [ src image_url ] [] ]

                                                    Nothing ->
                                                        p [] [ text (user.first_name ++ " " ++ user.last_name) ]

                                            Nothing ->
                                                div [] [ text "User not found!" ]

                                    Session.SignedIn _ ->
                                        div [] [ text "Validating..." ]

                                    Session.NotSignedIn ->
                                        div [] [ text "Sign In" ]

                                    Session.GoogleError _ ->
                                        div [] [ text "Google failed to sign in" ]
                                ]
                            , div [ class "navbar-dropdown is-right", classList [ ( "is-hidden", isValidated model.session ) ] ]
                                [ div [ class "navbar-item" ]
                                    [ div
                                        [ class "g-signin2"
                                        , attribute "data-onsuccess" "onSignIn"
                                        ]
                                        []
                                    ]
                                ]
                            , div [ class "navbar-dropdown is-right", classList [ ( "is-hidden", not (isValidated model.session) ) ] ]
                                [ a [ href "/", class "navbar-item", onClick SignOut ] [ text "Sign Out" ] ]
                            ]
                        ]
                    ]
                ]
            , div [ class "columns" ]
                [ div [ class "column is-one-fifth" ] []
                , div [ class "column" ] [ viewPage model ]
                , div [ class "column is-one-fifth" ] []
                ]
            ]
        ]
    }


viewUserList : Table.State -> Dict Id User -> Html Msg
viewUserList users_table users =
    div []
        [ p [ class "title has-text-centered" ] [ text "Users" ]
        , div [ class "columns" ]
            [ div [ class "column is-one-fifth" ]
                [ p [ class "title is-4 has-text-centered" ] [ text "Search" ]
                , p [ class "has-text-centered" ] [ text "Working on it :)" ]
                ]
            , div [ class "column" ]
                [ Table.view
                    users_table_config
                    users_table
                    (Dict.values users)
                ]
            ]
        ]


viewUser : User -> Html msg
viewUser user =
    a
        [ class "box"
        , href (B.relative [ "users", String.fromInt user.id ] [])
        ]
        [ p [ class "title is-5" ]
            [ text (user.first_name ++ " " ++ user.last_name) ]
        , p [ class "subtitle is-5 columns" ]
            [ span [ class "column" ]
                [ text ("Email: " ++ user.email) ]
            , span [ class "column" ]
                [ text ("Banner ID: " ++ String.fromInt user.banner_id) ]
            ]
        ]


viewEditableText : String -> Maybe String -> (String -> msg) -> msg -> Html msg
viewEditableText defaultText editedText onEdit onReset =
    case editedText of
        Nothing ->
            div [ class "field has-addons" ]
                [ div [ class "control is-expanded" ]
                    [ input
                        [ class "input"
                        , value defaultText
                        , readonly True
                        ]
                        []
                    ]
                , div [ class "control" ]
                    [ button
                        [ class "button"
                        , onClick (onEdit defaultText)
                        ]
                        [ text "Edit" ]
                    ]
                ]

        Just edited ->
            div [ class "field has-addons" ]
                [ div [ class "control is-expanded" ]
                    [ input
                        [ class "input is-focused"
                        , value edited
                        , onInput onEdit
                        ]
                        []
                    ]
                , div [ class "control" ]
                    [ button
                        [ class "button is-danger"
                        , onClick onReset
                        ]
                        [ text "Reset" ]
                    ]
                ]


viewEditableInt : Int -> Maybe Int -> (Maybe Int -> msg) -> msg -> Html msg
viewEditableInt default edited onEdit onReset =
    case edited of
        Nothing ->
            div [ class "field has-addons" ]
                [ div [ class "control is-expanded" ]
                    [ input
                        [ class "input"
                        , value (String.fromInt default)
                        , readonly True
                        ]
                        []
                    ]
                , div [ class "control" ]
                    [ button
                        [ class "button"
                        , onClick (onEdit (Just default))
                        ]
                        [ text "Edit" ]
                    ]
                ]

        Just edit ->
            div [ class "field has-addons" ]
                [ div [ class "control is-expanded" ]
                    [ input
                        [ class "input is-focused"
                        , value (String.fromInt edit)
                        , onInput (\s -> onEdit (String.toInt s))
                        ]
                        []
                    ]
                , div [ class "control" ]
                    [ button
                        [ class "button is-danger"
                        , onClick onReset
                        ]
                        [ text "Reset" ]
                    ]
                ]
