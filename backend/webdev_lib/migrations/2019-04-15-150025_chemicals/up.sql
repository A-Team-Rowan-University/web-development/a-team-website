-- Your SQL goes here
CREATE TABLE chemical (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    purpose VARCHAR(1023) NOT NULL,
    state_of_matter VARCHAR(255) NOT NULL,
    msds_path VARCHAR(1023) NOT NULL,
    qr_code VARCHAR(1023) NOT NULL,
    opended_life_span BIGINT UNSIGNED NOT NULL,
    unopend_life_span BIGINT UNSIGNED NOT NULL,
    is_controlled_substance BOOLEAN NOT NULL,
    is_restricted_substance BOOLEAN NOT NULL,
    is_petroleum_base BOOLEAN NOT NULL,
    signal_word VARCHAR(255) NOT NULL,
    company_name VARCHAR(255) NOT NULL,
    ingredients VARCHAR(1023) NOT NULL,
    manual_link VARCHAR(1023) NOT NULL
);

CREATE TABLE chemical_inventory (
    id SERIAL PRIMARY KEY,
    purchaser_id BIGINT UNSIGNED NOT NULL,
    custodian_id BIGINT UNSIGNED NOT NULL,
    chemical_id BIGINT UNSIGNED NOT NULL,
    storage_location VARCHAR(255) NOT NULL,
    lot_number VARCHAR(255) NOT NULL,
    purchase_date TIMESTAMP NOT NULL,
    arrival_date TIMESTAMP NOT NULL,
    open_date TIMESTAMP NOT NULL,
    expiration_date TIMESTAMP NOT NULL,
    disposal_date TIMESTAMP NOT NULL,
    removal_date TIMESTAMP NOT NULL,
    is_active BOOLEAN NOT NULL,
    container_type VARCHAR(255) NOT NULL,
    container_size VARCHAR(255) NOT NULL,
    unit VARCHAR(255) NOT NULL,
    percent_remaining DECIMAL NOT NULL,
    amount VARCHAR(255) NOT NULL,

    FOREIGN KEY (purchaser_id)
    REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    FOREIGN KEY (custodian_id)
    REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    FOREIGN KEY (chemical_id)
    REFERENCES chemical(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE chemical_precautions (
    id SERIAL PRIMARY KEY,
    chemical_id SERIAL NOT NULL,
    precaution_id SERIAL NOT NULL,

    FOREIGN KEY (chemical_id),
    REFERENCES chemicals(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    FOREIGN KEY (precaution_id),
    REFERENCES precautions(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE hazards (
    id SERIAL PRIMARY KEY,
    statement VARCHAR(255) NOT NULL
);

CREATE TABLE precautions (
    id SERIAL PRIMARY KEY,
    statement VARCHAR(255) NOT NUll
);

CREATE TABLE components (
    id SERIAL PRIMARY KEY,
    chemical_name VARCHAR(255) NOT NUll,
    common_name VARCHAR(255) NOT NULL,
    cas number VARCHAR(255) NOT NULL,
    substance number VARCHAR(255) NOT NULL,
    trade_secret_number VARCHAR(255) NOT NULL
);

CREATE TABLE pictograms (
    id SERIAL PRIMARY KEY,
    picture_name VARCHAR(255) NOT NULL,
    pictogram_path VARCHAR(255) NOT NULL
);

CREATE TABLE manufacturers (
    id SERIAL PRIMARY KEY,
    company_name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    phone_number VARCHAR(255) NOT NULL,
    website VARCHAR(255) NOT NULL
);

CREATE TABLE chemical_components (
    id SERIAL PRIMARY KEY,
    chemical_id BIGINT NOT NULL,
    component_id BIGINT NOT NULL,
    percentage DECIMAL NOT NULL,

    FOREIGN KEY(chemical_id)
    REFERENCES chemicals(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    FOREIGN KEY(component_id)
    REFERENCES components(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE manufacturer_chemicals (
    id SERIAL PRIMARY KEY,
    chemical_id SERIAL NOT NULL,
    manufacturer_id SERIAL NOT NULL,
    manufacturer_number VARCHAR(255),

    FOREIGN KEY (chemical_id),
    REFERENCES chemicals(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    FOREIGN KEY (manufacturer_id)
    REFERENCES manufacturers(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE chemical_pictograms (
    id SERIAL PRIMARY KEY,
    chemical_id BIGINT NOT NULL,
    pictogram_id BIGINT NOT NULL,

    FOREIGN KEY (chemical_id)
    REFERENCES chemicals(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    FOREIGN KEY (pictogram_id)
    REFERENCES pictograms(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE chemical_hazards (
    id SERIAL PRIMARY KEY,
    chemical_id BIGINT,
    hazard_id BIGINT,

    FOREIGN KEY (chemical_id)
    REFERENCES chemicals(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    FOREIGN KEY (hazard_id)
    REFERENCES hazards(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
