use calamine::open_workbook;
use calamine::RangeDeserializer;
use calamine::RangeDeserializerBuilder;
use calamine::Reader;
use calamine::Xls;
use graphql_client::GraphQLQuery;
use graphql_client::Response;
use reqwest;
use serde::de::Error;
use serde::Deserialize;
use serde::Deserializer;
use std::env;

#[derive(Deserialize, Debug)]
pub struct Id(#[serde(deserialize_with = "deserialize_id")] pub u64);

fn deserialize_id<'de, D>(d: D) -> Result<u64, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(d)?;
    s.parse().map_err(|e| D::Error::custom(e))
}

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/chemicals_schema.graphql",
    query_path = "src/chemicals_queries.graphql",
    response_derives = "Debug"
)]
pub struct CreateChemical;

#[derive(Deserialize, Debug)]
struct SheetChemical {
    building: String,
    room: String,
    major_location: String,
    minor_location: String,
    sublocation: String,
    product_name: String,
    manufacturer_number: String,
    manufacturer_name: String,
    manufacturer_address: String,
    manufacturer_city: String,
    manufacturer_state: String,
    manufacturer_zip: String,
    amt_of_product: f64,
    unit_of_measure: String,
    quantity: u64,
    container_type: String,
    ingredients: String,
    purpose: String,
    state_of_matter: String,
    petroleum_product: String,
    msds_link: String,
    signal_word: String,
    p_codes: String,
    h_codes: String,
    ghs_01: String,
    ghs_02: String,
    ghs_03: String,
    ghs_04: String,
    ghs_05: String,
    ghs_06: String,
    ghs_07: String,
    ghs_08: String,
    ghs_09: String,
    ghs01: String,
    ghs02: String,
    ghs03: String,
    ghs04: String,
    ghs05: String,
    ghs06: String,
    ghs07: String,
    ghs08: String,
    ghs09: String,
    sds_qrc: String,
}

fn main() {
    let mut args = env::args();

    // Get past the first arg of the program name
    let _ = args.next();

    let url = args.next().expect("No url argument!");

    let path = args.next().expect("No path argument!");

    let mut workbook: Xls<_> = open_workbook(path).expect("Could not open workbook");
    let range = workbook
        .worksheet_range("Chemical Inventory")
        .expect("Could not open sheet")
        .expect("Could not open sheet 2");

    let mut rows: RangeDeserializer<_, SheetChemical> = RangeDeserializerBuilder::new()
        .has_headers(false)
        .from_range(&range)
        .expect("Could not build range deserializer");

    // Skip the first row with headers
    let _ = rows.next();

    for row in rows {
        match row {
            Ok(sheet_chemical) => {
                let chemical = create_chemical::NewChemical {
                    company_name: sheet_chemical.manufacturer_name,
                    ingredients: sheet_chemical.ingredients,
                    is_controlled_substance: false,
                    is_petroleum_base: sheet_chemical.petroleum_product.contains('x'),
                    is_restricted_substance: false,
                    manual_link: String::new(),
                    msds_path: sheet_chemical.msds_link,
                    name: sheet_chemical.product_name,
                    opened_life_span: "Unknown".to_owned(),
                    purpose: sheet_chemical.purpose,
                    qr_code: sheet_chemical.sds_qrc,
                    signal_word: sheet_chemical.signal_word,
                    state_of_matter: sheet_chemical.state_of_matter,
                    unopened_life_span: "Unknown".to_owned(),
                };

                println!("==> Inserting chemical \"{}\"", chemical.name);

                let request_body =
                    CreateChemical::build_query(create_chemical::Variables { chemical });

                let client = reqwest::Client::new();
                let mut response = client.post(&url).json(&request_body).send().unwrap();

                let response_body: Response<create_chemical::ResponseData> =
                    response.json().unwrap();

                if let Some(errors) = response_body.errors {
                    println!("There were errors: ");
                    for err in errors {
                        println!("{:?}", err)
                    }
                } else {
                    println!("Success!");
                }
            }

            Err(e) => println!("Error converting chemical: {:?}", e),
        }
    }
}
