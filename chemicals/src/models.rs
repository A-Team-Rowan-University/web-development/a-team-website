use diesel::Queryable;
use diesel_derive_newtype::DieselNewType;

use juniper::parser::ParseError;
use juniper::parser::ScalarToken;
use juniper::parser::Token;
use juniper::GraphQLInputObject;
use juniper::GraphQLObject;
use juniper::ParseScalarResult;
use juniper::Value;

use chrono::NaiveDateTime;

use crate::schema::chemical_pictograms;
use crate::schema::chemicals;

#[derive(DieselNewType, PartialEq, Copy, Clone, Debug)]
pub struct Id(pub u64);

juniper::graphql_scalar!(Id as "Id" where Scalar = <S> {
    description: "A u64 id"

    resolve(&self) -> Value {
        Value::scalar(self.0.to_string())
    }

    from_input_value(v: &InputValue) -> Option<Id> {
        v.as_scalar_value().and_then(|s: &String| s.parse::<u64>().ok()).map(|s| Id(s))
    }

    from_str<'a>(value: ScalarToken<'a>) -> ParseScalarResult<'a, S> {
        if let ScalarToken::String(value) = value {
            Ok(S::from(value.to_owned()))
        } else {
            Err(ParseError::UnexpectedToken(Token::Scalar(value)))
        }
    }
});

#[derive(Queryable, GraphQLObject, Debug)]
pub struct Chemical {
    pub id: Id,                                   // N/A
    pub name: String,                             // Yes
    pub purpose: String,                          // Yes
    pub state_of_matter: String,                  // Yes
    pub msds_path: String,                        // Yes
    pub qr_code: String,                          // No
    pub opened_life_span: String,                 // No
    pub unopened_life_span: String,               // No
    pub is_controlled_substance: bool,            // No
    pub is_restricted_substance: bool,            // No
    pub is_petroleum_base: bool,                  // Yes
    pub signal_word: String,                      // Yes
    pub company_name: String,                     // No, manufacturer?
    pub ingredients: String,                      // Yes
    pub manual_link: String,                      // No
    pub components: Vec<ChemicalComponent>,       // No
    pub hazards: Vec<Hazard>,                     // No
    pub manufacturers: Vec<ChemicalManufacturer>, // Yes
    pub pictograms: Vec<Pictogram>,               // Yes
    pub precautions: Vec<Precaution>,             // Maybe
}

#[derive(Queryable, Debug)]
pub struct DbChemical {
    pub id: Id,
    pub name: String,
    pub purpose: String,
    pub state_of_matter: String,
    pub msds_path: String,
    pub qr_code: String,
    pub opened_life_span: String,
    pub unopened_life_span: String,
    pub is_controlled_substance: bool,
    pub is_restricted_substance: bool,
    pub is_petroleum_base: bool,
    pub signal_word: String,
    pub company_name: String,
    pub ingredients: String,
    pub manual_link: String,
}

#[derive(Insertable, GraphQLInputObject)]
#[table_name = "chemicals"]
pub struct NewChemical {
    pub name: String,
    pub purpose: String,
    pub state_of_matter: String,
    pub msds_path: String,
    pub qr_code: String,
    pub opened_life_span: String,
    pub unopened_life_span: String,
    pub is_controlled_substance: bool,
    pub is_restricted_substance: bool,
    pub is_petroleum_base: bool,
    pub signal_word: String,
    pub company_name: String,
    pub ingredients: String,
    pub manual_link: String,
}

#[derive(Queryable, GraphQLObject, Debug)]
pub struct ChemicalInventory {
    pub id: Id,                                 // N/A
    pub purchaser_id: Id,                       // No
    pub custodian_id: Id,                       // No
    pub chemical_id: Id,                        // Yes
    pub storage_location: String,               // Yes
    pub lot_number: String,                     // No
    pub purchase_date: Option<NaiveDateTime>,   // No
    pub arrival_date: Option<NaiveDateTime>,    // No
    pub open_date: Option<NaiveDateTime>,       // No
    pub expiration_date: Option<NaiveDateTime>, // No
    pub disposal_date: Option<NaiveDateTime>,   // No
    pub removal_date: Option<NaiveDateTime>,    // No
    pub is_active: bool,                        // No
    pub container_type: String,                 // Yes
    pub container_size: f64,                    // Yes
    pub unit: String,                           // Yes
    pub amount: f64,                            // Yes
}

#[derive(Queryable, GraphQLObject, Debug)]
pub struct Component {
    pub id: Id,                      // N/A
    pub chemical_name: String,       // No, are these the ingredients?
    pub common_name: String,         // No
    pub cas_number: String,          // No
    pub substance_number: String,    // No
    pub trade_secret_number: String, // No
}

#[derive(Queryable, GraphQLObject, Debug)]
pub struct ChemicalComponent {
    pub component: Component, // No
    pub percentage: f64,      // No
}

#[derive(Queryable, GraphQLObject, Debug)]
pub struct Hazard {
    pub id: Id,            // N/A
    pub statement: String, // No
}

#[derive(Queryable, GraphQLObject, Debug)]
pub struct Manufacturer {
    pub id: Id,               // N/A
    pub company_name: String, // Yes
    pub address: String,      // Yes
    pub phone_number: String, // No
    pub website: String,      // No
}

#[derive(Queryable, GraphQLObject, Debug)]
pub struct ChemicalManufacturer {
    pub manufacturer: Manufacturer,  // Yes
    pub manufacturer_number: String, // Yes
}

#[derive(Queryable, GraphQLObject, Debug)]
pub struct Pictogram {
    pub id: Id,       // N/A
    pub name: String, // Yes
    pub path: String, // Yes
}

#[derive(Insertable)]
#[table_name = "chemical_pictograms"]
pub struct DbChemicalPictogram {
    pub chemical_id: Id,
    pub pictogram_id: Id,
}

#[derive(Queryable, GraphQLObject, Debug)]
pub struct Precaution {
    pub id: Id,            // N/A
    pub statement: String, // Yes, also a code?
}
