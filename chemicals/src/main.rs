// Some diesel macros rely on the old way of importing macros
#[macro_use]
extern crate diesel_migrations;

#[macro_use]
extern crate diesel;

mod db;
mod graphql;
mod models;
mod schema;

use std::env;

use diesel::r2d2::ConnectionManager;
use diesel::MysqlConnection;
use r2d2::Pool;

use serde::Serialize;

use rouille::Request;
use rouille::Response;

use log::debug;
use log::error;
use log::info;
use log::warn;

static LOGO_SVG: &'static str = include_str!(concat!(env!("OUT_DIR"), "/static/logo.svg"));
static MAIN_HTML: &'static str = include_str!(concat!(env!("OUT_DIR"), "/static/main.html"));
static MAIN_JS: &'static str = include_str!(concat!(env!("OUT_DIR"), "/static/main.js"));

pub enum Error {
    CouldNotBuildConnectionPool,
    ConnectionPoolTimeout,

    GoogleSignIn(google_signin::Error),
    GoogleUserNoEmail,
    GoogleUserEmailNotFound(String),

    InvalidBody(serde_json::Error),
    NoBody,

    ChemicalNotFound(u64),
    NewChemicalInsert,
    NoChanges,

    Unknown(Box<dyn std::error::Error>),
}

impl Error {
    fn code(&self) -> u16 {
        match self {
            Error::CouldNotBuildConnectionPool => 0,
            Error::ConnectionPoolTimeout => 1,
            Error::GoogleSignIn(_) => 10,
            Error::GoogleUserNoEmail => 11,
            Error::GoogleUserEmailNotFound(_) => 12,
            Error::InvalidBody(_) => 20,
            Error::NoBody => 21,
            Error::ChemicalNotFound(_) => 30,
            Error::NewChemicalInsert => 32,
            Error::NoChanges => 33,
            Error::Unknown(_) => 99,
        }
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::CouldNotBuildConnectionPool => write!(
                f,
                "Could not get enough database connections to fill the connection pool"
            ),

            Error::ConnectionPoolTimeout => write!(
                f,
                "Timed out trying to get a connection from the connection pool"
            ),

            Error::GoogleSignIn(e) => match e {
                google_signin::Error::DecodeJson(e) => {
                    write!(f, "Google sign in returned invalid JSON: {}", e)
                }

                google_signin::Error::ConnectionError(e) => {
                    write!(f, "Could not connect to Google to validate token: {}", e)
                }

                _ => write!(f, "Invalid Google Signin token"),
            },

            Error::GoogleUserNoEmail => {
                write!(f, "Google did not provide an email for the signed-in user")
            }

            Error::GoogleUserEmailNotFound(email) => write!(
                f,
                "The email provided by Google does not match a user in the database: {}",
                email
            ),

            Error::InvalidBody(e) => write!(f, "{}", e),

            Error::NoBody => write!(f, "No body was provided with the request"),

            Error::ChemicalNotFound(id) => {
                write!(f, "The requested chemical was not found: {}", id)
            }

            Error::NewChemicalInsert => write!(f, "Could not insert a new chemical"),

            Error::NoChanges => write!(f, "No changes were requested"),

            Error::Unknown(e) => write!(f, "An unknown error occurred: {}", e),
        }
    }
}

impl From<r2d2::Error> for Error {
    fn from(_e: r2d2::Error) -> Error {
        Error::ConnectionPoolTimeout
    }
}

impl From<google_signin::Error> for Error {
    fn from(e: google_signin::Error) -> Error {
        Error::GoogleSignIn(e)
    }
}

impl From<diesel::result::Error> for Error {
    fn from(e: diesel::result::Error) -> Error {
        match e {
            diesel::result::Error::QueryBuilderError(_e) => Error::NoChanges,
            _ => Error::Unknown(Box::new(e)),
        }
    }
}

#[derive(Serialize, Debug)]
struct ErrorResponse {
    error_code: u16,
    error_description: String,
    http_code: u16,
}

impl From<Error> for Response {
    fn from(e: Error) -> Response {
        let http_code = match e {
            Error::CouldNotBuildConnectionPool => 500,
            Error::ConnectionPoolTimeout => 500,
            Error::GoogleSignIn(_) => 401,
            Error::GoogleUserNoEmail => 401,
            Error::GoogleUserEmailNotFound(_) => 401,
            Error::InvalidBody(_) => 400,
            Error::NoBody => 400,
            Error::ChemicalNotFound(_) => 404,
            Error::NewChemicalInsert => 500,
            Error::NoChanges => 400,
            Error::Unknown(_) => 500,
        };

        let error_response = ErrorResponse {
            error_code: e.code(),
            error_description: e.to_string(),
            http_code,
        };

        Response::json(&error_response).with_status_code(http_code)
    }
}

/*
fn validate_token(
    id_token: &str,
    database_connection: &MysqlConnection,
) -> Result<models::User, Error> {
    let mut client = google_signin::Client::new();
    client.audiences.push(String::from(
        "918184954544-jm1aufr31fi6sdjs1140p7p3rouaka14.apps.googleusercontent.com",
    ));

    let id_info = client.verify(id_token)?;

    debug!("Validated token: {:?}", id_info);

    if let Some(email) = id_info.email {
        let users = db::get_users(database_connection)?;

        let user = users.users.into_iter().find(|u| u.email == email);

        if let Some(user) = user {
            Ok(user)
        } else {
            Err(Error::GoogleUserEmailNotFound(email))
        }
    } else {
        Err(Error::GoogleUserNoEmail)
    }
}
*/

embed_migrations!("migrations");

fn init_database(url: &str) -> Result<Pool<ConnectionManager<MysqlConnection>>, Error> {
    info!("Connecting to database");
    let manager = ConnectionManager::new(url);

    let pool = Pool::builder().max_size(15).build(manager);

    let pool = match pool {
        Ok(p) => p,
        Err(_e) => return Err(Error::CouldNotBuildConnectionPool),
    };

    info!("Running migrations");
    if let Err(e) = embedded_migrations::run(&pool.get()?) {
        warn!("Could not run migrations: {}", e);
    }

    Ok(pool)
}

fn main() {
    let logging_config = simplelog::ConfigBuilder::new()
        .add_filter_ignore("hyper".to_string())
        .add_filter_ignore("rustls".to_string())
        .build();

    simplelog::SimpleLogger::init(simplelog::LevelFilter::Trace, logging_config)
        .expect("Could not initialize logging");

    info!("ConnectionManager to database");

    let database_url = match env::var("DATABASE_URL") {
        Ok(url) => url,
        Err(e) => {
            error!("Could not read DATABASE_URL environment variable: {}", e);
            return;
        }
    };

    let connection_pool = match init_database(&database_url) {
        Ok(pool) => pool,
        Err(e) => {
            error!("Could not connect to the database: {}", e);
            return;
        }
    };

    info!("Connected to database, starting server");

    rouille::start_server("0.0.0.0:8000", move |request| {
        debug!(
            "Handling request {} {} from {}",
            request.method(),
            request.raw_url(),
            request.remote_addr(),
        );

        if request.method() == "OPTIONS" {
            Response::text("")
                .with_additional_header(
                    "Access-Control-Allow-Methods",
                    "POST, GET, DELETE, OPTIONS, PUT",
                )
                .with_additional_header("Access-Control-Allow-Origin", "*")
                .with_additional_header(
                    "Access-Control-Allow-Headers",
                    "X-PINGOTHER, Content-Type, id_token",
                )
                .with_additional_header("Access-Control-Max-Age", "86400")
        } else {
            let response = handle_request(request, connection_pool.clone());

            response.with_additional_header("Access-Control-Allow-Origin", "*")
        }
    });
}

fn handle_request(
    request: &Request,
    connection_pool: Pool<ConnectionManager<MysqlConnection>>,
) -> Response {
    if let Some(graphql_request) = request.remove_prefix("/api/graphql") {
        let request_body = match graphql_request.data() {
            Some(d) => d,
            None => return Error::NoBody.into(),
        };

        let juniper_request: juniper::http::GraphQLRequest =
            match serde_json::from_reader(request_body) {
                Ok(r) => r,
                Err(e) => return Error::InvalidBody(e).into(),
            };

        let root_node = graphql::Schema::new(graphql::Query, graphql::Mutation);

        let context = graphql::Context {
            pool: connection_pool,
        };

        let juniper_response = juniper_request.execute(&root_node, &context);

        Response::json(&juniper_response)
    } else if let Some(_graphiql_request) = request.remove_prefix("/api/graphiql") {
        let html = juniper::graphiql::graphiql_source("/api/graphql");
        Response::html(html)
    } else {
        match request.url().as_ref() {
            "/main.js" => Response::text(MAIN_JS),
            "/logo.svg" => Response::svg(LOGO_SVG),
            "/favicon.ico" => Response::svg(LOGO_SVG),
            _ => Response::html(MAIN_HTML),
        }
    }
}
