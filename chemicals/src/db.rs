use diesel;
use diesel::mysql::MysqlConnection;
use diesel::NullableExpressionMethods;
use diesel::QueryDsl;
use diesel::RunQueryDsl;

use crate::Error;

use crate::models::Chemical;
use crate::models::ChemicalComponent;
use crate::models::ChemicalInventory;
use crate::models::ChemicalManufacturer;
use crate::models::Component;
use crate::models::DbChemical;
use crate::models::DbChemicalPictogram;
use crate::models::Hazard;
use crate::models::Id;
use crate::models::Manufacturer;
use crate::models::NewChemical;
use crate::models::Pictogram;
use crate::models::Precaution;

use crate::schema::chemical_components;
use crate::schema::chemical_hazards;
use crate::schema::chemical_inventory;
use crate::schema::chemical_manufacturers;
use crate::schema::chemical_pictograms;
use crate::schema::chemical_precautions;
use crate::schema::chemicals;
use crate::schema::components;
use crate::schema::hazards;
use crate::schema::manufacturers;
use crate::schema::pictograms;
use crate::schema::precautions;

pub fn get_chemicals(connection: &MysqlConnection) -> Result<Vec<Chemical>, Error> {
    let joined_chemicals = chemicals::table
        .left_join(chemical_components::table.left_join(components::table))
        .left_join(chemical_hazards::table.left_join(hazards::table))
        .left_join(chemical_manufacturers::table.left_join(manufacturers::table))
        .left_join(chemical_pictograms::table.left_join(pictograms::table))
        .left_join(chemical_precautions::table.left_join(precautions::table))
        .select((
            chemicals::all_columns,
            (components::all_columns, chemical_components::percentage).nullable(),
            hazards::all_columns.nullable(),
            (
                manufacturers::all_columns,
                chemical_manufacturers::manufacturer_number,
            )
                .nullable(),
            pictograms::all_columns.nullable(),
            precautions::all_columns.nullable(),
        ))
        .load::<(
            DbChemical,
            Option<ChemicalComponent>,
            Option<Hazard>,
            Option<ChemicalManufacturer>,
            Option<Pictogram>,
            Option<Precaution>,
        )>(connection)?;

    let mut chemicals: Vec<Chemical> = Vec::new();

    for (chemical, component, hazard, manufacturer, pictogram, precaution) in
        joined_chemicals.into_iter()
    {
        if let Some(existing_chemical) = chemicals.iter_mut().find(|c| c.id == chemical.id) {
            existing_chemical.components.extend(component);
            existing_chemical.hazards.extend(hazard);
            existing_chemical.manufacturers.extend(manufacturer);
            existing_chemical.pictograms.extend(pictogram);
            existing_chemical.precautions.extend(precaution);
        } else {
            let new_chemical = Chemical {
                id: chemical.id,
                name: chemical.name,
                purpose: chemical.purpose,
                state_of_matter: chemical.state_of_matter,
                msds_path: chemical.msds_path,
                qr_code: chemical.qr_code,
                opened_life_span: chemical.opened_life_span,
                unopened_life_span: chemical.unopened_life_span,
                is_controlled_substance: chemical.is_controlled_substance,
                is_restricted_substance: chemical.is_restricted_substance,
                is_petroleum_base: chemical.is_petroleum_base,
                signal_word: chemical.signal_word,
                company_name: chemical.company_name,
                ingredients: chemical.ingredients,
                manual_link: chemical.manual_link,
                components: component.into_iter().collect(),
                hazards: hazard.into_iter().collect(),
                manufacturers: manufacturer.into_iter().collect(),
                pictograms: pictogram.into_iter().collect(),
                precautions: precaution.into_iter().collect(),
            };

            chemicals.push(new_chemical)
        }
    }

    Ok(chemicals)
}

pub fn create_chemical(
    chemical: NewChemical,
    connection: &MysqlConnection,
) -> Result<Chemical, Error> {
    diesel::insert_into(chemicals::table)
        .values(chemical)
        .execute(connection)?;

    let mut inserted_chemicals = chemicals::table
        .filter(diesel::dsl::sql("id = LAST_INSERT_ID()"))
        .load::<DbChemical>(connection)?;

    if let Some(chemical) = inserted_chemicals.pop() {
        Ok(Chemical {
            id: chemical.id,
            name: chemical.name,
            purpose: chemical.purpose,
            state_of_matter: chemical.state_of_matter,
            msds_path: chemical.msds_path,
            qr_code: chemical.qr_code,
            opened_life_span: chemical.opened_life_span,
            unopened_life_span: chemical.unopened_life_span,
            is_controlled_substance: chemical.is_controlled_substance,
            is_restricted_substance: chemical.is_restricted_substance,
            is_petroleum_base: chemical.is_petroleum_base,
            signal_word: chemical.signal_word,
            company_name: chemical.company_name,
            ingredients: chemical.ingredients,
            manual_link: chemical.manual_link,
            components: Vec::new(),
            hazards: Vec::new(),
            manufacturers: Vec::new(),
            pictograms: Vec::new(),
            precautions: Vec::new(),
        })
    } else {
        Err(Error::NewChemicalInsert)
    }
}

pub fn add_pictogram_to_chemical(
    chemical_id: Id,
    pictogram_id: Id,
    connection: &MysqlConnection,
) -> Result<Chemical, Error> {
    diesel::insert_into(chemical_pictograms::table)
        .values(DbChemicalPictogram {
            chemical_id,
            pictogram_id,
        })
        .execute(connection)?;

    let mut chemical = get_chemicals(connection)?
        .into_iter()
        .find(|c| c.id == chemical_id);

    if let Some(chemical) = chemical {
        Ok(chemical)
    } else {
        Err(Error::ChemicalNotFound(chemical_id.0))
    }
}

pub fn get_pictograms(connection: &MysqlConnection) -> Result<Vec<Pictogram>, Error> {
    let pictograms = pictograms::table.load::<Pictogram>(connection)?;
    Ok(pictograms)
}
