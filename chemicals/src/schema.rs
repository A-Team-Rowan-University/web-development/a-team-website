
table! {
    chemicals (id) {
        id -> Unsigned<Bigint>,
        name -> Varchar,
        purpose -> Varchar,
        state_of_matter -> Varchar,
        msds_path -> Varchar,
        qr_code -> Varchar,
        opened_life_span -> Varchar,
        unopened_life_span -> Varchar,
        is_controlled_substance -> Bool,
        is_restricted_substance -> Bool,
        is_petroleum_base -> Bool,
        signal_word -> Varchar,
        company_name -> Varchar,
        ingredients -> Varchar,
        manual_link -> Varchar,
    }
}

table! {
    chemical_inventory (id) {
        id -> Unsigned<Bigint>,
        purchaser_id -> Unsigned<Bigint>,
        custodian_id -> Unsigned<Bigint>,
        chemical_id -> Unsigned<Bigint>,
        storage_location -> Varchar,
        lot_number -> Varchar,
        purchase_date -> Timestamp,
        arrival_date -> Timestamp,
        open_date -> Timestamp,
        expiration_date -> Timestamp,
        disposal_date -> Timestamp,
        removal_date -> Timestamp,
        is_active -> Bool,
        container_type -> Varchar,
        container_size -> Double,
        unit -> Varchar,
        amount -> Double,
    }
}

table! {
    chemical_precautions (chemical_id, precaution_id) {
        chemical_id -> Unsigned<Bigint>,
        precaution_id -> Unsigned<Bigint>,
    }
}

table! {
    hazards (id) {
        id -> Unsigned<Bigint>,
        statement -> Varchar,
    }
}

table! {
    precautions (id) {
        id -> Unsigned<Bigint>,
        statement -> Varchar,
    }
}

table! {
    components (id) {
        id -> Unsigned<Bigint>,
        chemical_name -> Varchar,
        common_name -> Varchar,
        cas_number -> Varchar,
        substance_number -> Varchar,
        trade_secret_number -> Varchar,
    }
}

table! {
    pictograms (id) {
        id -> Unsigned<Bigint>,
        name -> Varchar,
        path -> Varchar,
    }
}

table! {
    manufacturers (id) {
        id -> Unsigned<Bigint>,
        company_name -> Varchar,
        address -> Varchar,
        phone_number -> Varchar,
        website -> Varchar,
    }
}

table! {
    chemical_components (chemical_id, component_id) {
        chemical_id -> Unsigned<Bigint>,
        component_id -> Unsigned<Bigint>,
        percentage -> Double,
    }
}

table! {
    chemical_manufacturers (chemical_id, manufacturer_id) {
        chemical_id -> Unsigned<Bigint>,
        manufacturer_id -> Unsigned<Bigint>,
        manufacturer_number -> Varchar,
    }
}

table! {
    chemical_pictograms (chemical_id, pictogram_id) {
        chemical_id -> Unsigned<Bigint>,
        pictogram_id -> Unsigned<Bigint>,
    }
}

table! {
    chemical_hazards (chemical_id, hazard_id) {
        chemical_id -> Unsigned<Bigint>,
        hazard_id -> Unsigned<Bigint>,
    }
}

//Cant seem to do this because of multiple points to users, need explicit on clause in queries
//joinable!(chemical_inventory -> users (purchaser_id));
//joinable!(chemical_inventory -> users (custodian_id));

joinable!(chemical_inventory -> chemicals (chemical_id));
joinable!(chemical_precautions -> chemicals (chemical_id));
joinable!(chemical_precautions -> precautions (precaution_id));
joinable!(chemical_components -> chemicals (chemical_id));
joinable!(chemical_components -> components (component_id));
joinable!(chemical_manufacturers -> chemicals (chemical_id));
joinable!(chemical_manufacturers -> manufacturers (manufacturer_id));
joinable!(chemical_pictograms -> chemicals (chemical_id));
joinable!(chemical_pictograms -> pictograms (pictogram_id));
joinable!(chemical_hazards -> chemicals (chemical_id));
joinable!(chemical_hazards -> hazards (hazard_id));

allow_tables_to_appear_in_same_query!(chemicals, chemical_inventory, chemical_precautions, hazards, precautions, components, pictograms, manufacturers, chemical_components, chemical_manufacturers, chemical_pictograms, chemical_hazards);
