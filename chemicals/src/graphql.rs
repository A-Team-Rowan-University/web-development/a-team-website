use diesel::r2d2::ConnectionManager;
use diesel::MysqlConnection;
use r2d2::Pool;

use juniper;
use juniper::FieldResult;

use crate::db;
use crate::models::Chemical;
use crate::models::Id;
use crate::models::NewChemical;
use crate::models::Pictogram;

#[derive(Clone)]
pub struct Context {
    pub pool: Pool<ConnectionManager<MysqlConnection>>,
}

impl juniper::Context for Context {}

pub struct Query;

#[juniper::object(Context = Context)]
impl Query {
    fn chemicals(context: &Context) -> FieldResult<Vec<Chemical>> {
        let connection = context.pool.get()?;
        Ok(db::get_chemicals(&connection)?)
    }

    fn pictograms(context: &Context) -> FieldResult<Vec<Pictogram>> {
        let connection = context.pool.get()?;
        Ok(db::get_pictograms(&connection)?)
    }
}

pub struct Mutation;

#[juniper::object(Context = Context)]
impl Mutation {
    fn create_chemical(context: &Context, chemical: NewChemical) -> FieldResult<Chemical> {
        let connection = context.pool.get()?;
        Ok(db::create_chemical(chemical, &connection)?)
    }

    fn add_pictogram_to_chemical(
        context: &Context,
        chemical_id: Id,
        pictogram_id: Id,
    ) -> FieldResult<Chemical> {
        let connection = context.pool.get()?;
        Ok(db::add_pictogram_to_chemical(
            chemical_id,
            pictogram_id,
            &connection,
        )?)
    }
}

pub type Schema = juniper::RootNode<'static, Query, Mutation>;
