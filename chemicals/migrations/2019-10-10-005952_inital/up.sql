-- Your SQL goes here
CREATE TABLE chemicals (
                           id SERIAL PRIMARY KEY,
                           name VARCHAR(255) NOT NULL,
                           purpose VARCHAR(1023) NOT NULL,
                           state_of_matter VARCHAR(255) NOT NULL,
                           msds_path VARCHAR(1023) NOT NULL,
                           qr_code VARCHAR(1023) NOT NULL,
                           opened_life_span VARCHAR(255) NOT NULL,
                           unopened_life_span VARCHAR(255) NOT NULL,
                           is_controlled_substance BOOLEAN NOT NULL,
                           is_restricted_substance BOOLEAN NOT NULL,
                           is_petroleum_base BOOLEAN NOT NULL,
                           signal_word VARCHAR(255) NOT NULL,
                           company_name VARCHAR(255) NOT NULL,
                           ingredients VARCHAR(1023) NOT NULL,
                           manual_link VARCHAR(1023) NOT NULL
);

CREATE TABLE chemical_inventory (
                                    id SERIAL PRIMARY KEY,
                                    purchaser_id BIGINT UNSIGNED NOT NULL,
                                    custodian_id BIGINT UNSIGNED NOT NULL,
                                    chemical_id BIGINT UNSIGNED NOT NULL,
                                    storage_location VARCHAR(255) NOT NULL,
                                    lot_number VARCHAR(255) NOT NULL,
                                    purchase_date TIMESTAMP NOT NULL,
                                    arrival_date TIMESTAMP NOT NULL,
                                    open_date TIMESTAMP NOT NULL,
                                    expiration_date TIMESTAMP NOT NULL,
                                    disposal_date TIMESTAMP NOT NULL,
                                    removal_date TIMESTAMP NOT NULL,
                                    is_active BOOLEAN NOT NULL,
                                    container_type VARCHAR(255) NOT NULL,
                                    container_size DOUBLE NOT NULL,
                                    unit VARCHAR(255) NOT NULL,
                                    amount DOUBLE NOT NULL,

                                    FOREIGN KEY (chemical_id)
                                        REFERENCES chemicals(id)
                                        ON DELETE CASCADE
                                        ON UPDATE CASCADE
);

CREATE TABLE precautions (
                             id SERIAL PRIMARY KEY,
                             statement VARCHAR(255) NOT NUll
);

CREATE TABLE chemical_precautions (
                                      chemical_id BIGINT UNSIGNED NOT NULL,
                                      precaution_id BIGINT UNSIGNED NOT NULL,

                                      PRIMARY KEY (chemical_id, precaution_id),

                                      FOREIGN KEY (chemical_id)
                                          REFERENCES chemicals(id)
                                          ON DELETE CASCADE
                                          ON UPDATE CASCADE,

                                      FOREIGN KEY (precaution_id)
                                          REFERENCES precautions(id)
                                          ON DELETE CASCADE
                                          ON UPDATE CASCADE
);

CREATE TABLE components (
                            id SERIAL PRIMARY KEY,
                            chemical_name VARCHAR(255) NOT NUll,
                            common_name VARCHAR(255) NOT NULL,
                            cas_number VARCHAR(255) NOT NULL,
                            substance_number VARCHAR(255) NOT NULL,
                            trade_secret_number VARCHAR(255) NOT NULL
);

CREATE TABLE chemical_components (
                                     chemical_id BIGINT UNSIGNED NOT NULL,
                                     component_id BIGINT UNSIGNED NOT NULL,
                                     percentage DOUBLE NOT NULL,

                                     PRIMARY KEY (chemical_id, component_id),

                                     FOREIGN KEY(chemical_id)
                                         REFERENCES chemicals(id)
                                         ON DELETE CASCADE
                                         ON UPDATE CASCADE,

                                     FOREIGN KEY(component_id)
                                         REFERENCES components(id)
                                         ON DELETE CASCADE
                                         ON UPDATE CASCADE
);

CREATE TABLE pictograms (
                            id SERIAL PRIMARY KEY,
                            name VARCHAR(255) NOT NULL,
                            path VARCHAR(255) NOT NULL
);

CREATE TABLE chemical_pictograms (
                                     chemical_id BIGINT UNSIGNED NOT NULL,
                                     pictogram_id BIGINT UNSIGNED NOT NULL,

                                     PRIMARY KEY (chemical_id, pictogram_id),

                                     FOREIGN KEY (chemical_id)
                                         REFERENCES chemicals(id)
                                         ON DELETE CASCADE
                                         ON UPDATE CASCADE,

                                     FOREIGN KEY (pictogram_id)
                                         REFERENCES pictograms(id)
                                         ON DELETE CASCADE
                                         ON UPDATE CASCADE
);

CREATE TABLE manufacturers (
                               id SERIAL PRIMARY KEY,
                               company_name VARCHAR(255) NOT NULL,
                               address VARCHAR(255) NOT NULL,
                               phone_number VARCHAR(255) NOT NULL,
                               website VARCHAR(255) NOT NULL
);

CREATE TABLE chemical_manufacturers (
                                        chemical_id BIGINT UNSIGNED NOT NULL,
                                        manufacturer_id BIGINT UNSIGNED NOT NULL,
                                        manufacturer_number VARCHAR(255),

                                        PRIMARY KEY (chemical_id, manufacturer_id),

                                        FOREIGN KEY (chemical_id)
                                            REFERENCES chemicals(id)
                                            ON DELETE CASCADE
                                            ON UPDATE CASCADE,

                                        FOREIGN KEY (manufacturer_id)
                                            REFERENCES manufacturers(id)
                                            ON DELETE CASCADE
                                            ON UPDATE CASCADE
);

CREATE TABLE hazards (
                         id SERIAL PRIMARY KEY,
                         statement VARCHAR(1023)
);

CREATE TABLE chemical_hazards (
                                  chemical_id BIGINT UNSIGNED NOT NULL,
                                  hazard_id BIGINT UNSIGNED NOT NULL,

                                  PRIMARY KEY (chemical_id, hazard_id),

                                  FOREIGN KEY (chemical_id)
                                      REFERENCES chemicals(id)
                                      ON DELETE CASCADE
                                      ON UPDATE CASCADE,

                                  FOREIGN KEY (hazard_id)
                                      REFERENCES hazards(id)
                                      ON DELETE CASCADE
                                      ON UPDATE CASCADE
);
