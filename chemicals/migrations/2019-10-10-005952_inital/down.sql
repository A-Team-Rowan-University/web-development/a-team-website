-- This file should undo anything in `up.sql`
DROP TABLE chemical_components;
DROP TABLE chemical_manufacturers;
DROP TABLE chemical_pictograms;
DROP TABLE chemical_hazards;
DROP TABLE chemical_precautions;
DROP TABLE chemical_inventory;
DROP TABLE chemicals;
DROP TABLE hazards;
DROP TABLE precautions;
DROP TABLE components;
DROP TABLE pictograms;
DROP TABLE manufacturers;
