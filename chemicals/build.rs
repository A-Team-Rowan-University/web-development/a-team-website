use std::env;
use std::fs::copy;
use std::fs::create_dir;
use std::process::Command;

const FRONTEND_DIR: &str = "frontend";

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    if out_dir != FRONTEND_DIR {
        create_dir(format!("{}/static", out_dir));

        copy(
            &format!("{}/static/logo.svg", FRONTEND_DIR),
            &format!("{}/static/logo.svg", out_dir),
        )
        .expect("Could not copy logo.svg");

        copy(
            &format!("{}/static/main.html", FRONTEND_DIR),
            &format!("{}/static/main.html", out_dir),
        )
        .expect("Could not copy main.html");
    }

    if env::var("DO_NOT_BUILD_ELM").is_err() {
        let elm_cmd = env::var("ELM_CMD").unwrap_or("elm".to_owned());
        println!("Using elm: {}", elm_cmd);

        if !Command::new(elm_cmd)
            .args(&[
                "make",
                &format!("src/Main.elm"),
                "--output",
                &format!("{}/static/main.js", out_dir),
            ])
            .current_dir(FRONTEND_DIR)
            .status()
            .expect("Could not run elm")
            .success()
        {
            panic!("Frontend elm failed to compile");
        }
    }
}
