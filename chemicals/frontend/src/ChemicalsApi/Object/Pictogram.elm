-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module ChemicalsApi.Object.Pictogram exposing (..)

import ChemicalsApi.InputObject
import ChemicalsApi.Interface
import ChemicalsApi.Object
import ChemicalsApi.Scalar
import ChemicalsApi.Union
import CustomScalarCodecs
import Graphql.Internal.Builder.Argument as Argument exposing (Argument)
import Graphql.Internal.Builder.Object as Object
import Graphql.Internal.Encode as Encode exposing (Value)
import Graphql.Operation exposing (RootMutation, RootQuery, RootSubscription)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode


id : SelectionSet CustomScalarCodecs.Id ChemicalsApi.Object.Pictogram
id =
    Object.selectionForField "CustomScalarCodecs.Id" "id" [] (CustomScalarCodecs.codecs |> ChemicalsApi.Scalar.unwrapCodecs |> .codecId |> .decoder)


name : SelectionSet String ChemicalsApi.Object.Pictogram
name =
    Object.selectionForField "String" "name" [] Decode.string


path : SelectionSet String ChemicalsApi.Object.Pictogram
path =
    Object.selectionForField "String" "path" [] Decode.string
