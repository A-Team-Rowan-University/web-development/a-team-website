-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module ChemicalsApi.Object.Manufacturer exposing (..)

import ChemicalsApi.InputObject
import ChemicalsApi.Interface
import ChemicalsApi.Object
import ChemicalsApi.Scalar
import ChemicalsApi.Union
import CustomScalarCodecs
import Graphql.Internal.Builder.Argument as Argument exposing (Argument)
import Graphql.Internal.Builder.Object as Object
import Graphql.Internal.Encode as Encode exposing (Value)
import Graphql.Operation exposing (RootMutation, RootQuery, RootSubscription)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode


id : SelectionSet CustomScalarCodecs.Id ChemicalsApi.Object.Manufacturer
id =
    Object.selectionForField "CustomScalarCodecs.Id" "id" [] (CustomScalarCodecs.codecs |> ChemicalsApi.Scalar.unwrapCodecs |> .codecId |> .decoder)


companyName : SelectionSet String ChemicalsApi.Object.Manufacturer
companyName =
    Object.selectionForField "String" "companyName" [] Decode.string


address : SelectionSet String ChemicalsApi.Object.Manufacturer
address =
    Object.selectionForField "String" "address" [] Decode.string


phoneNumber : SelectionSet String ChemicalsApi.Object.Manufacturer
phoneNumber =
    Object.selectionForField "String" "phoneNumber" [] Decode.string


website : SelectionSet String ChemicalsApi.Object.Manufacturer
website =
    Object.selectionForField "String" "website" [] Decode.string
