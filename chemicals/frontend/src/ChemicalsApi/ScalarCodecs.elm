-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module ChemicalsApi.ScalarCodecs exposing (..)

import ChemicalsApi.Scalar exposing (defaultCodecs)
import Json.Decode as Decode exposing (Decoder)


type alias Id =
    ChemicalsApi.Scalar.Id


codecs : ChemicalsApi.Scalar.Codecs Id
codecs =
    ChemicalsApi.Scalar.defineCodecs
        { codecId = defaultCodecs.codecId
        }
