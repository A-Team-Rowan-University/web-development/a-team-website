module Main exposing (..)

--import ChemicalsApi.Mutation as Mutation

import Browser
import Browser.Navigation as Nav
import ChemicalsApi.Object
import ChemicalsApi.Object.Chemical as ChemicalApi
import ChemicalsApi.Object.ChemicalComponent as ChemicalComponentApi
import ChemicalsApi.Object.ChemicalManufacturer as ChemicalManufacturerApi
import ChemicalsApi.Object.Component as ComponentApi
import ChemicalsApi.Object.Hazard as HazardApi
import ChemicalsApi.Object.Manufacturer as ManufacturerApi
import ChemicalsApi.Object.Pictogram as PictogramApi
import ChemicalsApi.Object.Precaution as PrecautionApi
import ChemicalsApi.Query as Query
import Dict exposing (Dict)
import Graphql.Http
import Graphql.Operation exposing (RootMutation, RootQuery)
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet)
import Html exposing (Html, a, div, img, nav, p, span, text)
import Html.Attributes exposing (attribute, class, classList, href, id, src)
import Html.Events exposing (onClick)
import Table exposing (defaultCustomizations)
import Url
import Url.Builder as B
import Url.Parser as P exposing ((</>))



-- Types


type alias Id =
    Int


type alias Chemical =
    { id : Id
    , name : String
    , purpose : String
    , state_of_matter : String
    , msds_path : String
    , qr_code : String
    , opened_life_span : String
    , unopened_life_span : String
    , is_controlled_substance : Bool
    , is_restricted_substance : Bool
    , is_petroleum_base : Bool
    , signal_word : String
    , company_name : String
    , ingredients : String
    , manual_link : String
    , components : List ChemicalComponent
    , hazards : List Id
    , manufacturers : List ChemicalManufacturer
    , pictograms : List Id
    , precautions : List Id
    }


type alias Component =
    { id : Id
    , chemical_name : String
    , common_name : String
    , cas_number : String
    , substance_number : String
    , trade_secret_number : String
    }


type alias ChemicalComponent =
    { component : Id
    , percentage : Float
    }


type alias Hazard =
    { id : Id
    , statement : String
    }


type alias Manufacturer =
    { id : Id
    , company_name : String
    , address : String
    , phone_number : String
    , website : String
    }


type alias ChemicalManufacturer =
    { manufacturer : Id
    , manufacturer_number : String
    }


type alias Pictogram =
    { id : Id
    , name : String
    , path : String
    }


type alias Precaution =
    { id : Id
    , statement : String
    }



-- Queries and Mutations


queryChemicals : SelectionSet (List Chemical) RootQuery
queryChemicals =
    Query.chemicals
        (SelectionSet.succeed Chemical
            |> SelectionSet.with ChemicalApi.id
            |> SelectionSet.with ChemicalApi.name
            |> SelectionSet.with ChemicalApi.purpose
            |> SelectionSet.with ChemicalApi.stateOfMatter
            |> SelectionSet.with ChemicalApi.msdsPath
            |> SelectionSet.with ChemicalApi.qrCode
            |> SelectionSet.with ChemicalApi.openedLifeSpan
            |> SelectionSet.with ChemicalApi.unopenedLifeSpan
            |> SelectionSet.with ChemicalApi.isControlledSubstance
            |> SelectionSet.with ChemicalApi.isRestrictedSubstance
            |> SelectionSet.with ChemicalApi.isPetroleumBase
            |> SelectionSet.with ChemicalApi.signalWord
            |> SelectionSet.with ChemicalApi.companyName
            |> SelectionSet.with ChemicalApi.ingredients
            |> SelectionSet.with ChemicalApi.manualLink
            |> SelectionSet.with (ChemicalApi.components selectChemicalComponent)
            |> SelectionSet.with (ChemicalApi.hazards HazardApi.id)
            |> SelectionSet.with (ChemicalApi.manufacturers selectChemicalManufacturer)
            |> SelectionSet.with (ChemicalApi.pictograms PictogramApi.id)
            |> SelectionSet.with (ChemicalApi.precautions PrecautionApi.id)
        )


selectChemicalComponent : SelectionSet ChemicalComponent ChemicalsApi.Object.ChemicalComponent
selectChemicalComponent =
    SelectionSet.map2 ChemicalComponent
        (ChemicalComponentApi.component ComponentApi.id)
        ChemicalComponentApi.percentage


selectComponent : SelectionSet Component ChemicalsApi.Object.Component
selectComponent =
    SelectionSet.map6 Component
        ComponentApi.id
        ComponentApi.chemicalName
        ComponentApi.commonName
        ComponentApi.casNumber
        ComponentApi.substanceNumber
        ComponentApi.tradeSecretNumber


selectHazard : SelectionSet Hazard ChemicalsApi.Object.Hazard
selectHazard =
    SelectionSet.map2 Hazard HazardApi.id HazardApi.statement


selectChemicalManufacturer : SelectionSet ChemicalManufacturer ChemicalsApi.Object.ChemicalManufacturer
selectChemicalManufacturer =
    SelectionSet.map2 ChemicalManufacturer
        (ChemicalManufacturerApi.manufacturer ManufacturerApi.id)
        ChemicalManufacturerApi.manufacturerNumber


selectManufacturer : SelectionSet Manufacturer ChemicalsApi.Object.Manufacturer
selectManufacturer =
    SelectionSet.map5 Manufacturer
        ManufacturerApi.id
        ManufacturerApi.address
        ManufacturerApi.companyName
        ManufacturerApi.phoneNumber
        ManufacturerApi.website


queryPictograms : SelectionSet (List Pictogram) RootQuery
queryPictograms =
    Query.pictograms
        (SelectionSet.map3
            Pictogram
            PictogramApi.id
            PictogramApi.name
            PictogramApi.path
        )


selectPrecaution : SelectionSet Precaution ChemicalsApi.Object.Precaution
selectPrecaution =
    SelectionSet.map2 Precaution
        PrecautionApi.id
        PrecautionApi.statement


boolColumn : String -> (data -> Bool) -> Table.Column data msg
boolColumn name to_bool =
    Table.customColumn
        { name = name
        , viewData =
            \d ->
                if to_bool d then
                    "YES"

                else
                    "NO"
        , sorter =
            Table.increasingOrDecreasingBy
                (\d ->
                    if to_bool d then
                        1

                    else
                        0
                )
        }


urlColumn : String -> (data -> String) -> (data -> String) -> Table.Column data msg
urlColumn name to_url to_string =
    Table.veryCustomColumn
        { name = name
        , viewData =
            \d ->
                { attributes = []
                , children = [ a [ href (to_url d) ] [ text (to_string d) ] ]
                }
        , sorter = Table.increasingOrDecreasingBy to_string
        }



-- Tables config


chemicals_table_config : Table.Config ( Bool, Chemical ) Msg
chemicals_table_config =
    Table.customConfig
        { toId = \( s, chemical ) -> String.fromInt chemical.id
        , toMsg = ChemicalsTable
        , columns =
            [ urlColumn "Name" (\( s, c ) -> B.absolute [ "chemicals", String.fromInt c.id ] []) (\( s, c ) -> c.name)
            , Table.stringColumn "Purpose" (\( s, c ) -> c.purpose)
            , Table.stringColumn "State of Matter" (\( s, c ) -> c.state_of_matter)
            , Table.stringColumn "MSDS" (\( s, c ) -> c.msds_path)
            , Table.stringColumn "QR Code" (\( s, c ) -> c.qr_code)
            , Table.stringColumn "Opened Life Span" (\( s, c ) -> c.opened_life_span)
            , Table.stringColumn "Unopened Life Span" (\( s, c ) -> c.unopened_life_span)
            , boolColumn "Controlled?" (\( s, c ) -> c.is_controlled_substance)
            , boolColumn "Restricted?" (\( s, c ) -> c.is_restricted_substance)
            , boolColumn "Petroleum Base?" (\( s, c ) -> c.is_petroleum_base)
            , Table.stringColumn "Signal Word" (\( s, c ) -> c.signal_word)
            , Table.stringColumn "Company" (\( s, c ) -> c.company_name)
            , Table.stringColumn "Ingredients" (\( s, c ) -> c.ingredients)
            , Table.stringColumn "Manual Link" (\( s, c ) -> c.manual_link)
            ]
        , customizations =
            { defaultCustomizations
                | tableAttrs = [ class "table is-fullwidth is-hoverable is-striped" ]
                , rowAttrs = \( s, c ) -> [ classList [ ( "is-selected", s ) ] ]
            }
        }


pictograms_table_config : Table.Config Pictogram Msg
pictograms_table_config =
    Table.customConfig
        { toId = \pictogram -> String.fromInt pictogram.id
        , toMsg = PictogramsTable
        , columns =
            [ Table.stringColumn "Name" .name
            , Table.stringColumn "Path" .path
            ]
        , customizations = { defaultCustomizations | tableAttrs = [ class "table is-fullwidth is-hoverable is-striped" ] }
        }



-- Elm App


main =
    Browser.application
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


type alias Model =
    { navkey : Nav.Key
    , route : Route
    , chemicals : Dict Int Chemical
    , chemicals_table : Table.State
    , pictograms : Dict Int Pictogram
    , pictograms_table : Table.State
    , burger_open : Bool
    }


type Msg
    = GotChemicals (Result (Graphql.Http.Error (List Chemical)) (List Chemical))
    | GotPictograms (Result (Graphql.Http.Error (List Pictogram)) (List Pictogram))
    | ChemicalsTable Table.State
    | PictogramsTable Table.State
    | BurgerToggle
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url


type Route
    = ChemicalsPage (Maybe Id)
    | PictogramsPage (Maybe Id)
    | PageNotFound


parseRoute : P.Parser (Route -> a) a
parseRoute =
    P.oneOf
        [ P.map (ChemicalsPage Nothing) P.top
        , P.map (ChemicalsPage Nothing) (P.s "chemicals")
        , P.map (\id -> ChemicalsPage (Just id)) (P.s "chemicals" </> P.int)
        , P.map (PictogramsPage Nothing) (P.s "pictograms")
        , P.map (\id -> PictogramsPage (Just id)) (P.s "pictograms" </> P.int)
        ]


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( { navkey = key
      , route = Maybe.withDefault PageNotFound (P.parse parseRoute url)
      , chemicals =
            Dict.empty
      , chemicals_table = Table.initialSort "Name"
      , pictograms = Dict.empty
      , pictograms_table = Table.initialSort "Name"
      , burger_open = False
      }
    , load_data (ChemicalsPage Nothing)
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotChemicals (Ok chemicals) ->
            ( { model | chemicals = List.map (\chemical -> ( chemical.id, chemical )) chemicals |> Dict.fromList }, Cmd.none )

        GotChemicals (Err err) ->
            ( model, Cmd.none )

        GotPictograms (Ok pictograms) ->
            ( { model | pictograms = List.map (\pictogram -> ( pictogram.id, pictogram )) pictograms |> Dict.fromList }, Cmd.none )

        GotPictograms (Err err) ->
            ( model, Cmd.none )

        ChemicalsTable state ->
            ( { model | chemicals_table = state }, Cmd.none )

        PictogramsTable state ->
            ( { model | pictograms_table = state }, Cmd.none )

        BurgerToggle ->
            ( { model | burger_open = not model.burger_open }, Cmd.none )

        LinkClicked (Browser.Internal url) ->
            ( model, Nav.pushUrl model.navkey (Url.toString url) )

        LinkClicked (Browser.External href) ->
            ( model, Nav.load href )

        UrlChanged url ->
            case P.parse parseRoute url of
                Nothing ->
                    ( { model | route = PageNotFound }, Cmd.none )

                Just route ->
                    ( { model | route = route }, load_data route )


load_data : Route -> Cmd Msg
load_data route =
    case route of
        ChemicalsPage _ ->
            queryChemicals
                |> Graphql.Http.queryRequest "/api/graphql"
                |> Graphql.Http.send GotChemicals

        PictogramsPage _ ->
            queryPictograms
                |> Graphql.Http.queryRequest "/api/graphql"
                |> Graphql.Http.send GotPictograms

        PageNotFound ->
            Cmd.none


view : Model -> Browser.Document Msg
view model =
    { title = "Chemicals"
    , body =
        [ div []
            [ viewNav BurgerToggle model.burger_open
            , div [] <|
                case model.route of
                    ChemicalsPage (Just id) ->
                        [ p [ class "title has-text-centered" ] [ text "Chemicals" ]
                        , viewTable chemicals_table_config model.chemicals_table (Dict.map (\i c -> ( i == id, c )) model.chemicals) ]

                    ChemicalsPage Nothing ->
                        [ viewTable chemicals_table_config model.chemicals_table (Dict.map (\i c -> ( False, c )) model.chemicals) ]

                    PictogramsPage _ ->
                        [p[ class "title has-text-centered" ] [text "Pictograms"]
                        ,viewTable pictograms_table_config model.pictograms_table model.pictograms ]
                    

                    PageNotFound ->
                        [ p [] [ text "This page does not exist" ] ]
            ]
        ]
    }


viewNav : msg -> Bool -> Html msg
viewNav on_toggle burger_open =
    nav [ class "navbar", class "is-primary" ]
        [ div [ class "navbar-brand" ]
            [ a [ class "navbar-item", href "/" ]
                [ img
                    [ src
                        (B.absolute [ "logo.svg" ] [])
                    ]
                    []
                ]
            , a
                [ attribute "role" "button"
                , href ""
                , class "navbar-burger"
                , class "burger"
                , attribute "aria-label" "menu"
                , attribute "aria-expanded" "false"
                , attribute "data-target" "navbar"
                , onClick on_toggle
                , classList [ ( "is-active", burger_open ) ]
                ]
                [ span [ attribute "aria-hidden" "true" ] []
                , span [ attribute "aria-hidden" "true" ] []
                , span [ attribute "aria-hidden" "true" ] []
                ]
            ]
        , div
            [ id "navbar"
            , class "navbar-menu"
            , classList [ ( "is-active", burger_open ) ]
            ]
            [ div [ class "navbar-start" ]
                [ a [ class "navbar-item", href "/chemicals" ]
                    [ text "Chemicals" ]
                , a [ class "navbar-item", href "/pictograms" ]
                    [ text "Pictograms" ]
                ]
            ]
        ]


viewTable : Table.Config data msg -> Table.State -> Dict Id data -> Html msg
viewTable table_config table_state data =
    div [ class "table-container" ]
        [ Table.view table_config table_state (Dict.values data) ]
