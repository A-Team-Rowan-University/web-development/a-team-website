module CustomScalarCodecs exposing (..)

import ChemicalsApi.Scalar
import Json.Decode as Decode
import Json.Encode as Encode


type alias Id =
    Int


codecs : ChemicalsApi.Scalar.Codecs Id
codecs =
    ChemicalsApi.Scalar.defineCodecs
        { codecId =
            { encoder = \raw -> raw |> String.fromInt |> Encode.string
            , decoder =
                Decode.string
                    |> Decode.map String.toInt
                    |> Decode.andThen
                        (\maybeParsedId ->
                            case maybeParsedId of
                                Just parsedId ->
                                    Decode.succeed parsedId

                                Nothing ->
                                    Decode.fail "Could not parse Id as an Int"
                        )
            }
        }
